<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::localizedGroup(function ()  {
        //front routes
        Route::get('/', 'HomeController@index')->name('front.home');
        Route::get('/statements', 'ProductsController@index')->name('front.statements');
        Route::get('/statements/searchresults', 'ProductsController@index')->name('front.statements.search');
        //Route::get('/statements/search', 'ProductsController@SearchStatements')->name('front.statements.search');
        Route::get('/statements/add', 'OrdersController@AddStatement')->name('front.statements.add');
        Route::post('/statements/add', 'OrdersController@AddStatementPost')->name('front.statements.addpost');
        Route::get('/statements/detail/{id}/{slug?}', 'ProductsController@Show')->name('front.statements.detail');

        Route::get('/services', 'ServicesController@index')->name('front.services');
        Route::get('/services/detail/{id}/{slug?}', 'ServicesController@Show')->name('front.services.detail');
        Route::get('/about', 'AboutController@index')->name('front.about');
        Route::get('/blog', 'BlogController@index')->name('front.blog');
        Route::get('/blog/detail/{id}/{slug?}', 'BlogController@Show')->name('front.blog.detail');
        Route::get('/advertisement', 'HomeController@index')->name('front.advertisement');
        Route::get('/contact', 'HomeController@index')->name('front.contact');
        Route::post('/contact/sendmail', 'ContactController@SendMail')->name('contact.sendmail');
        Route::post('/contact/send', 'ContactController@SendMailAjax')->name('contact.send');


        //admin routes
        Route::prefix('admin')->group(function () {
            Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
            Route::post('/login', 'Auth\AdminLoginController@login')->name('loguser');
            Route::group(['middleware' => ['auth:admin','ClearanceMiddleware']], function () {

                Route::get('/', 'AdminController@index')->name('admin_home');
                Route::post('/savetasks', 'AdminController@SaveTasks')->name('savetasks');
                Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
                Route::get('/viewlistitem/{id}/{table}', 'AdminController@view_list_item');
                Route::post('/addlistitem', 'AdminController@add_list_item')->name('addlistitem');
                Route::post('/editlistitem', 'AdminController@edit_list_item')->name('editlistitem');
                Route::post('/deletelistitem/{id}/{table}', 'AdminController@DeleteListItem')->name('deletelistitem');
                Route::post('/deletepost/{id}/{table}', 'AdminController@DeletePost')->name('deleteitem');
                Route::post('/changelistitemstatus/{id}/{table}', 'AdminController@ChangeListItemStatus')->name('changestatus');
                Route::post('/changestatus', 'AdminController@ChangeStatus')->name('changepoststatus');
                Route::post('/changecarstatus', 'AdminController@ChangecarStatus')->name('changecarstatus');
                Route::post('/changemainstatus', 'AdminController@ChangeMainStatus')->name('changemainstatus');
                Route::post('/changelistmainstatus', 'AdminController@ChangeListMainStatus')->name('changelistmainstatus');
                Route::post('/changelistitemsortorder', 'AdminController@ChangeListItemSortOrder')->name('changesortorder');
                Route::post('/changetableitemsortorder', 'AdminController@ChangeTableItemSortOrder')->name('changetablesortorder');
                Route::prefix('locations')->group(function () {
                    //ajax locations
                    Route::get('/loadlocations', 'ProductsController@LoadLocations')->name('admin.loadlocations');
                    //ajax locations
                    Route::post('/regions', 'LocationsController@RegionsAjax')->name('admin.locations.regions');
                    //ajax locations
                    Route::post('/districts', 'LocationsController@DistrictsAjax')->name('admin.locations.districts');
                    //ajax locations
                    Route::post('/urban', 'LocationsController@UrbanAjax')->name('admin.locations.urban');
                    //ajax locations
                    Route::post('/streets', 'LocationsController@StreetsAjax')->name('admin.locations.streets');
                    //ajax locations
                    Route::get('/regions', 'LocationsController@Regions')->name('admin.locations.regions');
                    Route::get('/regions/add', 'LocationsController@addRegions')->name('admin.locations.regions.add');
                    Route::get('/regions/edit/{id}', 'LocationsController@RegionsById')->name('admin.locations.regions.edit');
                    //ajax locations
                    Route::get('/districts', 'LocationsController@Districts')->name('admin.locations.districts');
                    Route::get('/districts/add', 'LocationsController@addDistricts')->name('admin.locations.districts.add');
                    Route::get('/districts/edit/{id}', 'LocationsController@DistrictsById')->name('admin.locations.districts.edit');
                    //ajax locations
                    Route::get('/urban', 'LocationsController@Urban')->name('admin.locations.urban');
                    Route::get('/urban/add', 'LocationsController@addUrban')->name('admin.locations.urban.add');
                    Route::get('/urban/edit/{id}', 'LocationsController@UrbanById')->name('admin.locations.urban.edit');
                    //ajax locations
                    Route::get('/streets', 'LocationsController@Streets')->name('admin.locations.streets');
                    Route::get('/streets/add', 'LocationsController@addStreets')->name('admin.locations.streets.add');
                    Route::get('/streets/edit/{id}', 'LocationsController@StreetsById')->name('admin.locations.streets.edit');
                });

                Route::post('/locations/edit', 'LocationsController@EditLocationsPost')->name('admin.locations.editpost');
                Route::post('/locations/add', 'LocationsController@AddLocationsPost')->name('admin.locations.addpost');

                //ajax locations
                Route::post('/productsajax', 'ProductsController@ProductsAjax')->name('admin.productsajax');

                //attrs
                Route::get('/viewitem/{id}/{table}', 'AdminController@showeditmodal')->name('admin.editmodal');
                Route::post('/edititem', 'AdminController@editmodalpost')->name('admin.edititempost');

                //prattrs
                Route::get('/prattrs/{id}', 'CategoriesController@index')->name('admin.property_attrs');
                //prtypes
                Route::get('/prtypes', 'CategoriesController@PropertyCategories')->name('admin.property_types');

                //dltypes
                Route::get('/dltypes', 'CategoriesController@DealCategories')->name('admin.deal_types');

                //blog
                Route::get('/blog', 'BlogController@Blog')->name('admin.blog');
                Route::get('/blog/add', 'BlogController@addBlog')->name('admin.blog.add');
                Route::post('/blog', 'BlogController@AddBlogPost')->name('admin.blog.addpost');
                Route::get('/blog/edit/{id}', 'BlogController@BlogById')->name('admin.blog.edit');
                Route::post('/blog/edit', 'BlogController@EditBlogPost')->name('admin.blog.editpost');

                //users
                Route::get('/users', 'AdminUsersController@Users')->name('admin.users');
                Route::get('/users/add', 'AdminUsersController@addUsers')->name('admin.users.add');
                Route::post('/users', 'AdminUsersController@AddUsersPost')->name('admin.users.addpost');
                Route::get('/users/edit/{id}', 'AdminUsersController@UsersById')->name('admin.users.edit');
                Route::post('/users/edit', 'AdminUsersController@EditUsersPost')->name('admin.users.editpost');

                //products
                Route::get('/products', 'ProductsController@Products')->name('admin.products');
                Route::get('/products/add/{id}', 'ProductsController@addProducts')->name('admin.products.add');
                Route::post('/products', 'ProductsController@AddProductsPost')->name('admin.products.addpost');
                Route::get('/products/edit/{id}', 'ProductsController@ProductsById')->name('admin.products.edit');
                Route::post('/products/edit', 'ProductsController@EditProductsPost')->name('admin.products.editpost');

                //orders
                Route::get('/orders/checked', 'OrdersController@CheckedOrders')->name('admin.orders.checked');
                Route::get('/orders/notchecked', 'OrdersController@NotCheckedOrders')->name('admin.orders.notchecked');
                Route::get('/orders/edit/{id}', 'OrdersController@OrdersById')->name('admin.orders.edit');
                Route::post('/orders/edit', 'OrdersController@EditOrdersPost')->name('admin.orders.editpost');

                //services
                Route::get('/services', 'ServicesController@Services')->name('admin.services');
                Route::get('/services/add', 'ServicesController@addServices')->name('admin.services.add');
                Route::post('/services', 'ServicesController@AddServicesPost')->name('admin.services.addpost');
                Route::get('/services/edit/{id}', 'ServicesController@ServicesById')->name('admin.services.edit');
                Route::post('/services/edit', 'ServicesController@EditServicesPost')->name('admin.services.editpost');

                //about
                Route::get('/about', 'AboutController@About')->name('admin.about');
                Route::get('/about/add', 'AboutController@addAbout')->name('admin.about.add');
                Route::post('/about', 'AboutController@AddAboutPost')->name('admin.about.addpost');
                Route::get('/about/edit/{id}', 'AboutController@AboutById')->name('admin.about.edit');
                Route::post('/about/edit', 'AboutController@EditAboutPost')->name('admin.about.editpost');

                //meta
                Route::get('/meta', 'MetaController@Meta')->name('admin.meta');
                Route::get('/meta/add', 'MetaController@addMeta')->name('admin.meta.add');
                Route::post('/meta', 'MetaController@AddMetaPost')->name('admin.meta.addpost');
                Route::get('/meta/edit/{id}', 'MetaController@MetaById')->name('admin.meta.edit');
                Route::post('/meta/edit', 'MetaController@EditMetaPost')->name('admin.meta.editpost');

                //CONTACT
                Route::get('/contact/edit/{id}', 'ContactController@ContactById')->name('admin.contact.edit');
                Route::post('/contact/edit', 'ContactController@EditContactPost')->name('admin.contact.editpost');

                //translation routes
                Route::get('/translations/view/{groupKey?}',  ['uses' => '\Barryvdh\TranslationManager\Controller@getView'])->where('groupKey', '.*');
                Route::get('/translations/{groupKey?}', ['uses' => '\Barryvdh\TranslationManager\Controller@getIndex'])->where('groupKey', '.*')->name('admin.translations');
                Route::post('/translations/add/{groupKey}', ['uses' => '\Barryvdh\TranslationManager\Controller@postAdd'])->where('groupKey', '.*');
                Route::post('/translations/edit/{groupKey}', ['uses' => '\Barryvdh\TranslationManager\Controller@postEdit'])->where('groupKey', '.*');
                Route::post('/translations/groups/add', ['uses' => '\Barryvdh\TranslationManager\Controller@postAddGroup']);
                Route::post('/translations/delete/{groupKey}/{translationKey}', ['uses' => '\Barryvdh\TranslationManager\Controller@postDelete'])->where('groupKey', '.*');
                Route::post('/translations/import', ['uses' => '\Barryvdh\TranslationManager\Controller@postImport']);
                Route::post('/translations/find', ['uses' => '\Barryvdh\TranslationManager\Controller@postFind']);
                Route::post('/translations/locales/add', ['uses' => '\Barryvdh\TranslationManager\Controller@postAddLocale']);
                Route::post('/translations/locales/remove', ['uses' => '\Barryvdh\TranslationManager\Controller@postRemoveLocale']);
                Route::post('/translations/publish/{groupKey}', ['uses' => '\Barryvdh\TranslationManager\Controller@postPublish'])->where('groupKey', '.*');
                Route::post('/translations/translate-missing', ['uses' => '\Barryvdh\TranslationManager\Controller@postTranslateMissing']);

                //publish translations for using in JS files
                Route::get('/langstojs', function() {
                    Artisan::call('lang:js');
                })->name('langstojs');

                Route::post('/uploadfiles', 'FileController@UploadFiles')->name('admin.uploadfiles');

            });

        });

    });
