$(document).ready(function(){
    if($('.location-bl').length === 1){
        $('footer').removeClass('transparent');
    }else{
        $('footer').addClass('transparent');
    }
    $('.float-slider .owl-carousel').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots: false,
        smartSpeed: 500,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1.5
            },
            768:{
                items:2.5
            },
            1340:{
                items:3.5
            },
            1680:{
                items:4.5
            }
        }
    });
    $('.slide-prev').click(function(){
        $(this).parents('.find-owl').find('.owl-carousel').trigger('prev.owl.carousel');
    });
    $('.slide-next').click(function(){
        $(this).parents('.find-owl').find('.owl-carousel').trigger('next.owl.carousel');
    });
    $('.switch').click(function(){
        $('.switch').each(function(){
            if($(this).find('input').is(':checked')){
                $(this).find('input').attr('checked', false);
            }else{
                $(this).find('input').attr('checked', true);
            }
            // console.log($(this).find('input').attr('checked'));
        });
    });
    $('.dropdown .selected').click(function(){
        $(this).parents('.dropdown').toggleClass('active');
        console.log( $(this).parents('.dropdown').find('li').length );
        $(this).parents('.dropdown').find('.result li').length
        if ( $(this).parents('.dropdown').find('li').length > 4 ) {
			$(this).parents('.dropdown').find('.result').addClass('overflow-scroll');
        }
    });
    $('.dropdown .result li').click(function(){
        var text = $(this).text();
        $(this).parents('.dropdown').removeClass('active');
        $(this).parents('.dropdown').find('.text').text(text);
        var thisValue = $(this).data('value');
        $(this).parents('.dropdown').find('input[type="hidden"]').val(thisValue);
        console.log(thisValue)
    });
    $(document).mouseup(function(e){
    var container = $(".dropdown.active .result");
        if (!container.is(e.target) && container.has(e.target).length === 0){
            setTimeout(() => {
                container.parents('.dropdown').removeClass('active');
            }, 10);
        }
    });
    if($('#slider').length == 1 ){
        var minVal = $('.rangeslider #minPrice').data('value');
        var maxVal = $('.rangeslider #maxPrice').data('value');
        var rangeSlider = $('#slider');
        $('.rangeslider .fromVal').val(minVal);
        $('.rangeslider .toVal').val(maxVal);
        rangeSlider.slider({
          range: true,
          min: minVal,
          max: maxVal,
          values: [minVal, maxVal],
          slide: function (event, ui) {
            $('#minPrice').text('₾ ' + ui.values[0]);
            $('#maxPrice').text('₾ ' + ui.values[1]);
            $('.rangeslider .fromVal').val(ui.values[0]);
            $('.rangeslider .toVal').val(ui.values[1]);
          },
        });
        setTimeout(() => {
            $('#minPrice').val('₾ ' + minVal);
            $('#maxPrice').val('₾ ' + maxVal);
        }, 1);
        $('#minPrice').val(rangeSlider.slider('values', 0));
        $('#maxPrice').val(rangeSlider.slider('values', 1));

        $("#minPrice").change(function () {
          $(rangeSlider).slider('values', 0, $(this).val());
        });
        $("#maxPrice").change(function () {
          $(rangeSlider).slider('values', 1, $(this).val());
        });
        function checkPlaces() {
            var left = $('.ui-slider-handle').first().css('left');
            var right = $('.ui-slider-handle:last-child').css('right');
            $('.rangeVal.left').css('left', left);
            $('.rangeVal.right').css('right', right);
            console.log(left, right)
        }
        checkPlaces();
    }
    $('.detail-page .owl-carousel.first').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots: false,
        smartSpeed: 500,
        responsive:{
            0:{
                items:1
            },
        }
    })
    $('.detail-page .owl-carousel.sec').owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        dots: false,
        smartSpeed: 500,
        responsive:{
            0:{
                items:2
            },
            480:{
                items:3
            },
            580:{
                items:4
            },
            991:{
                items:6
            },
        }
    })
    $('.detail-page .owl-carousel.first').on('changed.owl.carousel', function() {
        setTimeout(() => {
            var activeID = $('.owl-item.active .item').data('id');
            $('.detail-page .sec.owl-carousel .item').removeClass('active');
            $('.owl-carousel.sec').find("[data-id='" + activeID + "']").addClass('active');
            console.log($('.owl-carousel.sec .owl-item.active .item').data('id'));
            console.log($('.owl-carousel.first .owl-item.active .item').data('id'));
        }, 100);
    })
    $('.detail-page .sec.owl-carousel .item').click(function(){
        var thisID = $(this).attr('data-id');
        $('.detail-page .owl-carousel.first').trigger('to.owl.carousel', thisID - 1);
        $('.detail-page .sec.owl-carousel .item').removeClass('active');
        $(this).addClass('active');
    })
    if($('.detail-page .sec.owl-carousel .item:not(.cloned)').length > 6){
        $('.detail-page .slide-btn').show();
    }else{
        $('.detail-page .slide-btn').hide();
    }
    $('.menu-bar').click(function(){
        $('.mob-menu').addClass('active');
    })
    $('.cancel').click(function(){
        $('.mob-menu').removeClass('active');
    });
    $(document).mouseup(function(e){
    var container = $(".mob-menu.active");
        if (!container.is(e.target) && container.has(e.target).length === 0){
            setTimeout(() => {
                container.removeClass('active');
            }, 10);
        }
    });

});
var _form = {
    checkFields: function(obj, callback) {
        var error = false;
        var field = false;
        var name = '';
        var parent;
        if ($(".dropzone")[0]){
            var myDropzone = Dropzone.forElement(".dropzone");
        }

        $.each($(obj).find(':text:visible, :password:visible, select:visible, textarea:visible'), function() { /*:checkbox:visible,*/
            parent = $(this).parent('p').length ? $(this).parent('p') : $(this).parents('div').first();
            if (($(this).is(':checkbox') && !$(this).is(':checked')) ||
                ($(this).data('error') && $.trim($(this).val()) == '') ||
                ($(this).data('type') == 'email' && !isEmail($(this).val())) ||
                ($(this).data('type') == 'int' && !isInt($(this).val())) ||
                ($(this).data('type') == 'double' && !isFloat($(this).val())) ||
                ($(this).data('minlength') && $.trim($(this).val()).length < $(this).data('minlength')) ||
                ($(this).data('min') && $(this).val() <= $(this).data('min')) ||
                ($(this).data('lengths') && $(this).data('lengths').indexOf($.trim($(this).val()).length) == -1) ||
                ($(this).data('equals') && $(this).val() != $(obj).find('[name="' + $(this).data('equals') + '"]').val())
            ) {
                name = $(this).attr('name') != undefined ? $(this).attr('name').replace('[]', '') : '';
                parent.next('#input-error-' + name).remove();
                parent.after('<p class="input-error" id="input-error-' + name + '">' + $(this).data('error') + '</p>');
                error = true;
                field = field == false ? $(this) : field;
            } else {
                name = $(this).attr('name') != undefined ? $(this).attr('name').replace('[]', '') : '';
                parent.next('#input-error-' + name).remove();
            }
        });
        if (field) {
            field.focus();
        }
        if (!error) {
            if ($(".dropzone")[0]){
                var file = myDropzone.files;
            }

            var formdata = new FormData();
            if ($(".dropzone")[0]){
                let ins = file.length;
                for (let x = 0; x < ins; x++) {
                    if(file && !(file[x] instanceof File)){
                        Object.keys(file[x]).forEach(key => {
                            let image = 'File['+x+']'+'['+(key)+']';
                            formdata.append(image, file[x][key]);
                        });
                    }
                }
            }

            $.each($(obj).serializeArray(), function(k,v){
                formdata.append(v.name, v.value);
            });
            $.ajax({
                url: $(obj).attr('action'),
                data: formdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    if ((data = _form.parseJSON(data)) === false) {
                        return false;
                    }
                    if ((data = _form.parseJSON(data)) === false) {
                        return false;
                    }

                    if (data.StatusCode == 0) {
                        $.toast({
                            heading: Lang.get('JsTrans.error'),
                            text: data.StatusMessage,
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'error',
                            hideAfter: 4000,
                            stack: 6
                        });
                        return false;
                    }else if (data.StatusCode == 3) {
                        $.each(data.StatusMessage, function(i) {
                            $.toast({
                                heading: Lang.get('JsTrans.error'),
                                text: data.StatusMessage[i],
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 10000,
                                stack: 10
                            });
                        });

                        return false;
                    }
                    _form.response = data;
                    if (typeof callback == 'function') {
                        callback();
                    } else {
                        $.toast({
                            heading: Lang.get('JsTrans.error'),
                            text: Lang.get('JsTrans.error_appeared'),
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'error',
                            hideAfter: 2500,
                            stack: 6
                        });
                    }
                }
            });
        }
        return error;
    },
    removeInputErrors: function(obj) {
        $(obj).find('.input-error').remove();
    },
    parseJSON: function(data) {
        try {
            data = $.parseJSON(data);
        } catch (e) {}
        if (['boolean', 'number', 'string', 'symbol', 'function'].indexOf(typeof data) == -1) {
            return data;
        } else {
            swal('Error');
            return false;
        }
    },
    focus: function() {
        var text = $('form').last().find(':text').first();
        var val = text.val();
        text.val('').val(val).focus();
    }
};
var front = {
    savePost: function(e, t) {
    return _form.checkFields(e, function() {
        if (_form.response.StatusCode === 1) {
            swal({
                title: "Good job!",
                text: _form.response.StatusMessage,
                type: "success",
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            }, function(){
                window.location.replace($(e).data("success-url"));
            });
        } else {
            alert(_form.response.StatusMessage);
        }
    }), !1
}
};

