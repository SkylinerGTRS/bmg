<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UsersPostRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    public function username()
    {
        return 'username';
    }

    protected function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(response()->json(['StatusCode' => 3, 'StatusMessage' => $validator->errors()->getMessages()]));
    }

    public function rules($id = '')
    {
        return [
            'Name'  => 'required|string|max:255',
             $this->username()  => 'sometimes|string|email|max:255|unique:admins',
            'Password' => 'sometimes|nullable|string|min:8|confirmed',
            'RoleName' => 'required|string',
        ];
    }

    public function attributes ($id = '') {
        return [
            'Name'  => \Lang::get('global.name'),
             $this->username()   => \Lang::get('global.email'),
            'Password' => \Lang::get('global.password'),
        ];
    }

}
