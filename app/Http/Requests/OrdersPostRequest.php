<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Lang;

class OrdersPostRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['StatusCode' => 3, 'StatusMessage' => $validator->errors()->getMessages()]));
    }

    public function rules()
    {
        return [
            "Name" => 'required|string',
            "Lastname" => 'required|string',
            "Email" => 'required|email',
            "Phone" => 'required|numeric',
            "Code" => 'required|string',
            "Price" => 'required|numeric',
            "Message" => 'required|string',
            "Terms" => 'required',
        ];
    }

    public function messages(){
        return [
        'Terms.required' => Lang::get('global.please_agree_terms'),
        ];
    }

}
