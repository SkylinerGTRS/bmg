<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LocationsPostRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['StatusCode' => 3, 'StatusMessage' => $validator->errors()->getMessages()]));
    }

    public function rules()
    {
        $rules = [
            'SubID' => 'sometimes|required',
        ];
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        foreach ($supportedLocalekeys as $locale) {
            $rules['Title-' . $locale] = 'required|string|max:255';
        }
        return $rules;
    }

}
