<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminAuthRequest extends FormRequest
{

    public function authorize()
    {
        if (!auth()->check()){
            return true;
        }
        return false;
    }
    public function username()
    {
        return 'username';
    }
    public function rules()
    {
        return [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ];
    }
}
