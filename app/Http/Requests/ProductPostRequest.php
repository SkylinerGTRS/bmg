<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductPostRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['StatusCode' => 3, 'StatusMessage' => $validator->errors()->getMessages()]));
    }

    public function rules()
    {
        $rules = [
                  "regionId" => 'required|numeric',
                  "districtId" => 'required|numeric',
                  "urbanId" => 'required|numeric',
                  "streetId" => 'required|numeric',
                  "DealId" => 'required|numeric',
                  "price_type" => 'required|numeric',
                  "space" => 'required|numeric',
                  "price_usd" => 'required|numeric',
                  "price_gel" => 'required|numeric',
        ];
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        foreach ($supportedLocalekeys as $locale) {
            $rules['Title-' . $locale] = 'required|string|max:255';
            $rules['Address-' . $locale] = 'required|string|max:255';
        }
        return $rules;
    }

}
