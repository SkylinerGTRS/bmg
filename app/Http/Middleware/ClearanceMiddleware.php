<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::user()->hasRole('super-admin')) //If user has this //permission
        {
            return $next($request);
        }
        $Permissions = DB::table('permissions')->get();
        $Locale = localization()->getCurrentLocale();
        foreach ($Permissions as $item) {
            if ($request->is($Locale.'/'.$item->pattern))
            {
                if (!Auth::user()->hasPermissionTo($item->name))
                {
                    abort('401');
                }
                else {
                    return $next($request);
                }
            }
        }

        return $next($request);
    }
}
