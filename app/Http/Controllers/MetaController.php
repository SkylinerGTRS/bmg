<?php

namespace App\Http\Controllers;

use App\Http\Requests\MetaPostRequest;
use App\Repositories\BaseRepository;
use App\Services\MetaServices;

class MetaController extends BaseController
{

    private $metaServices;
    private $baseRepository;
    public function __construct(MetaServices $metaServices, BaseRepository $baseRepository)
    {
        $this->middleware('auth:admin');
        $this->metaServices = $metaServices;
        $this->baseRepository = $baseRepository;
    }

    public function Meta()
    {
        $Meta = $this->metaServices->Back();
        return view('admin.meta.index',['Meta' => $Meta]);
    }

    public function addMeta()
    {
        return view('admin.meta.add');
    }

    public function AddMetaPost(MetaPostRequest $request)
    {
        return $this->metaServices->Create($request);
    }

    public function MetaById(int $PostID){
        $item = $this->metaServices->Show($PostID);
        $Files = $this->baseRepository->GetFiles($PostID,'meta');
        return view('admin.meta.edit',
            ['item' => $item,'Files' => $Files]);
    }

    public function EditMetaPost(MetaPostRequest $request)
    {
        return $this->metaServices->Update($request);
    }
}
