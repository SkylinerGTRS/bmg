<?php

namespace App\Http\Controllers;

use App;
use App\Models\Admin;
use App\Services\AdminServices;
use Illuminate\Http\Request;

class AdminController extends BaseController
{
    private $adminServices;
    public function __construct(AdminServices $adminServices)
    {
        $this->middleware('auth:admin');
        $this->adminServices = $adminServices;
    }

    public function index()
    {

        return view('admin.index');
    }

    public function SaveTasks(Request $request) {
        return $this->adminServices->SaveTasks($request);
    }


    public function add_list_item(Request $request) {
        return $this->adminServices->AddListItem($request);
    }


    public function view_list_item(int $PostID, string $PostTable){
        return $this->adminServices->ViewListItem($PostID, $PostTable);
    }

    public function showeditmodal($PostID, $PostTable){
        return $this->adminServices->ShowEditModal($PostID, $PostTable);
    }

    public function editmodalpost(Request $request){
        return $this->adminServices->EditModalPost($request);
    }

    public function edit_list_item(Request $request) {
        return $this->adminServices->EditListItem($request);
    }

    public function DeleteListItem(int $PostID, string $PostTable)
    {
        return $this->adminServices->DeleteListItem($PostID, $PostTable);
    }

    private function UpdateSubListItems(int $ParentID)
    {
        return $this->adminServices->UpdateSubListItems($ParentID);
    }

    public function ChangeListItemStatus(int $PostID, string $PostTable)
    {
        return $this->adminServices->ChangeListItemStatus($PostID, $PostTable);
    }

    public function ChangeStatus(Request $request)
    {
        return $this->adminServices->ChangeStatus($request);
    }

    public function ChangeSliderStatus(Request $request)
    {
        return $this->adminServices->ChangeSliderStatus($request);
    }

    public function ChangeMainStatus(Request $request)
    {
        return $this->adminServices->ChangeMainStatus($request);

    }

    public function ChangeListMainStatus(Request $request)
    {
        return $this->adminServices->ChangeListMainStatus($request);

    }

    public function DeletePost(Request $request)
    {
        return $this->adminServices->DeletePost($request);
    }


    public function ChangeListItemSortOrder(Request $request)
    {
        return $this->adminServices->ChangeListItemSortOrder($request);
    }

    public function ChangeTableItemSortOrder(Request $request)
    {
        return $this->adminServices->ChangeTableItemSortOrder($request);
    }
}


