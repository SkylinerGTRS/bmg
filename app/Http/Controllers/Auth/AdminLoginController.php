<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAuthRequest;
use App\Services\AdminAuthServices;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    private $adminServices;
    public function __construct(AdminAuthServices $adminServices)
    {
        $this->middleware('guest:admin')->except('logout');
        $this->adminServices = $adminServices;
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(AdminAuthRequest $request)
    {
       return $this->adminServices->login($request);
    }

    public function logout(Request $request)
    {
        return $this->adminServices->Logout($request);
    }
}
