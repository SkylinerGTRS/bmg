<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogPostRequest;
use App\Repositories\BaseRepository;
use App\Services\BlogServices;

class BlogController extends BaseController
{

    private $blogServices;
    private $baseRepository;
    public function __construct(BlogServices $blogServices,BaseRepository $baseRepository)
    {
        //$this->middleware('auth:admin');
        $this->blogServices = $blogServices;
        $this->baseRepository = $baseRepository;
    }

    public function index(){
        $meta = $this->baseRepository->GetMeta(7);
        $MainBlog = $this->blogServices->MainBlog();
        $Blog = $this->blogServices->Front($MainBlog->id,12);
        return view('front.blog.index', ['Blog' => $Blog, 'Meta' => $meta, 'MainBlog' => $MainBlog]);
    }
    public function Blog()
    {
        $Blog = $this->blogServices->Back();
        return view('admin.blog.index', ['Blog' => $Blog]);
    }
    public function Show(int $PostID){
        $Blog = $this->blogServices->ShowF($PostID);
        $Blogs = $this->blogServices->ShowWithLimit($PostID, 3);
        return view('front.blog.detail', ['item' => $Blog, 'Blogs' => $Blogs ]);
    }

    public function addBlog()
    {
        return view('admin.blog.add');
    }

    public function AddBlogPost(BlogPostRequest $request)
    {
        $this->blogServices->Create($request);
    }

    public function BlogById($PostID){

        $item = $this->blogServices->ShowB($PostID);
        $Files = $this->baseRepository->GetFiles($PostID, 'blog');
        return view('admin.blog.edit', ['item' => $item,'Files' => $Files]);
    }

    public function EditBlogPost(BlogPostRequest $request)
    {
        $this->blogServices->Update($request);
    }

}
