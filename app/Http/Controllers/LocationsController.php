<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationsPostRequest;
use App\Http\Requests\ProductPostRequest;
use App\Repositories\BaseRepository;
use App\Services\LocationsServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationsController extends BaseController
{

    private $locatonsServices;
    private $baseRepository;
    public function __construct(LocationsServices $locatonsServices,BaseRepository $baseRepository)
    {
        $this->middleware('auth:admin');
        $this->locatonsServices = $locatonsServices;
        $this->baseRepository = $baseRepository;
    }

    public function Regions()
    {
        return view('admin.locations.regions');
    }

    public function RegionsAjax(Request $request)
    {
        return $this->locatonsServices->Regions($request);
    }

    public function addRegions()
    {
        return view('admin.locations.add',['PostTo' => 'regions','PostFrom' => null]);
    }

    public function RegionsById(int $PostID){

        $item = $this->locatonsServices->RegionsById($PostID);

        return view('admin.locations.edit', ['item' => $item, 'PostTo' => 'regions','PostFrom' => null]);
    }

    public function Districts()
    {
        $Regions = DB::table('regions')->where('status_id', '!=', '2')->get();
        return view('admin.locations.districts',['Regions' => $Regions]);
    }

    public function DistrictsAjax(Request $request)
    {
        return $this->locatonsServices->Districts($request);
    }

    public function addDistricts()
    {
        $Regions = DB::table('regions')->where('status_id', '!=', '2')->get();
        return view('admin.locations.add', ['PostTo' => 'districts', 'PostFrom' => 'regions', 'SubLoc' => $Regions]);
    }

    public function DistrictsById(int $PostID){

        $item = $this->locatonsServices->DistrictsById($PostID);
        $Regions = DB::table('regions')->where('status_id', '!=', '2')->get();
        return view('admin.locations.edit', ['item' => $item, 'PostTo' => 'districts', 'PostFrom' => 'regions', 'SubLoc' => $Regions]);
    }

    public function Urban()
    {
        $Districts = DB::table('districts')->where('status_id', '!=', '2')->get();
        return view('admin.locations.urban',['Districts' => $Districts]);
    }

    public function UrbanAjax(Request $request)
    {
        return $this->locatonsServices->Urban($request);
    }

    public function addUrban()
    {
        $Districts = DB::table('districts')->where('status_id', '!=', '2')->get();
        return view('admin.locations.add', ['PostTo' => 'urban', 'PostFrom' => 'districts', 'SubLoc' => $Districts]);
    }

    public function UrbanById(int $PostID){

        $item = $this->locatonsServices->UrbanById($PostID);
        $Districts = DB::table('districts')->where('status_id', '!=', '2')->get();
        return view('admin.locations.edit', ['item' => $item, 'PostTo' => 'urban', 'PostFrom' => 'districts', 'SubLoc' => $Districts]);
    }

    public function Streets()
    {
        $Urban = DB::table('urban')->where('status_id', '!=', '2')->get();
        return view('admin.locations.streets',['Urban' => $Urban]);
    }

    public function StreetsAjax(Request $request)
    {
        return $this->locatonsServices->Streets($request);
    }

    public function addStreets()
    {
        return view('admin.locations.add', ['PostTo' => 'streets', 'PostFrom' => 'urban']);
    }

    public function AddLocationsPost(LocationsPostRequest $request)
    {
        $this->locatonsServices->Create($request);
    }

    public function StreetsById(int $PostID){

        $item = $this->locatonsServices->StreetsById($PostID);
        $SUrban = DB::table('urban')->find($item->urban_id);
        return view('admin.locations.edit', ['item' => $item, 'PostTo' => 'streets', 'PostFrom' => 'urban', 'Surban' => $SUrban]);
    }

    public function EditLocationsPost(LocationsPostRequest $request)
    {
        $this->locatonsServices->Update($request);
    }

}
