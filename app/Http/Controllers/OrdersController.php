<?php

namespace App\Http\Controllers;


use App\Http\Requests\OrdersPostRequest;
use App\Repositories\BaseRepository;
use App\Services\OrdersServices;
use Illuminate\Http\Request;

class OrdersController extends BaseController
{

    private $orderServices;
    private $baseRepository;
    public function __construct(OrdersServices $orderServices,BaseRepository $baseRepository)
    {
        //$this->middleware('auth:admin');
        $this->orderServices = $orderServices;
        $this->baseRepository = $baseRepository;
    }

    public function AddStatement()
    {
        $meta = $this->baseRepository->GetMeta(6);
        return view('front.products.add', ['Meta' => $meta]);
    }

    public function AddStatementPost(OrdersPostRequest $request)
    {
        return $this->orderServices->AddStatementPost($request);
    }

    public function EditOrdersPost(Request $request)
    {
        return $this->orderServices->EditOrdersPost($request);
    }

    public function CheckedOrders()
    {
        $Orders = $this->orderServices->CheckedOrders();
        return view('admin.orders.index', ['Orders' => $Orders]);
    }

    public function NotCheckedOrders()
    {
        $Orders = $this->orderServices->NotCheckedOrders();
        return view('admin.orders.index', ['Orders' => $Orders]);
    }

    public function OrdersById(int $ID)
    {
        $item = $this->orderServices->OrdersById($ID);
        $Files = $this->baseRepository->GetFiles($ID, 'orders');
        return view('admin.orders.edit', ['item' => $item, 'Files' => $Files]);
    }

}
