<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicesPostRequest;
use App\Repositories\BaseRepository;
use App\Services\Services;

class ServicesController extends BaseController
{

    private $services;
    private $baseRepository;
    public function __construct(Services $services,BaseRepository $baseRepository)
    {
        //$this->middleware('auth:admin');
        $this->services = $services;
        $this->baseRepository = $baseRepository;
    }

    public function index(){
        $meta = $this->baseRepository->GetMeta(2);
        $Services = $this->services->Front();
        return view('front.services.index', ['Services' => $Services, 'Meta' => $meta]);
    }
    public function Services()
    {
        $Services = $this->services->Back();
        return view('admin.services.index', ['Services' => $Services]);
    }
    public function Show(int $PostID){
        $item = $this->services->ShowF($PostID);
        $Services = $this->baseRepository->ServicesByLimit(5,$item->id);
        return view('front.services.detail', ['item' => $item, 'Services' => $Services]);
    }

    public function addServices()
    {
        return view('admin.services.add');
    }

    public function AddServicesPost(ServicesPostRequest $request)
    {
        $this->services->Create($request);
    }

    public function ServicesById($PostID){

        $item = $this->services->ShowB($PostID);
        $Files = $this->baseRepository->GetFiles($PostID, 'services');
        return view('admin.services.edit', ['item' => $item,'Files' => $Files]);
    }

    public function EditServicesPost(ServicesPostRequest $request)
    {
        $this->services->Update($request);
    }

}
