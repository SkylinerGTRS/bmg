<?php

namespace App\Http\Controllers;

use App\Models\DealCategories;
use App\Models\PropertyCategories;
use App\Models\Categories;
use App\Helpers\Helpers;

class CategoriesController extends BaseController
{
    public function index($PostID)
    {
        $locale = localization()->getCurrentLocale();
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        $Cats = Categories::orderBy('parent_id')
            ->where('type_id',$PostID)
            ->orderBy('sort_order','asc')->get();
        $Attrs = json_decode(json_encode($Cats), true);

        //dd($Cats);
        return view('admin.list.index',[
            'list' 			=> Helpers::TraverseCats($Attrs),
            'list_params' 	=> [
                'post_table'		=> 'categories',
                'list_title' 		=> 'ქონების ატრიბუტები',
                'add_item_title' 	=> 'დამატება',
                'type_id' 	        => $PostID,
                'add_sub_item_title'=> 'დამატება',
                'types' => true,
                'Icon'              => 'Icon',
                'edit_item_title' 	=> 'Edit Menu Item',
                'options'			=> ['edit', 'sort', 'status','delete', 'is_main']
            ], 'Langs' => $supportedLocalekeys,'Locale' => $locale]);

    }

    public function PropertyCategories()
    {
        $locale = localization()->getCurrentLocale();
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        $Cats = PropertyCategories::orderBy('parent_id')
            ->orderBy('sort_order','asc')->get();
        $Attrs = json_decode(json_encode($Cats), true);

        return view('admin.list.index',[
            'list' 			=> Helpers::TraverseCats($Attrs),
            'list_params' 	=> [
                'post_table'		=> 'propertycategories',
                'list_title' 		=> 'ქონების ტიპები',
                'add_item_title' 	=> 'დამატება',
                'types' => false,
                //'add_sub_item_title'=> 'დამატება',
                //'Icon'              => 'Icon',
                'edit_item_title' 	=> 'Edit Menu Item',
                'options'			=> ['edit', 'sort', 'status','delete']
            ], 'Langs' => $supportedLocalekeys,'Locale' => $locale]);

    }

    public function DealCategories()
    {
        $locale = localization()->getCurrentLocale();
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        $Cats = DealCategories::orderBy('parent_id')
            ->orderBy('sort_order','asc')->get();
        $Attrs = json_decode(json_encode($Cats), true);

        return view('admin.list.index',[
            'list' 			=> Helpers::TraverseCats($Attrs),
            'list_params' 	=> [
                'post_table'		=> 'dealcategories',
                'list_title' 		=> 'გარიგების ტიპები',
                'add_item_title' 	=> 'დამატება',
                'types' => false,
                //'add_sub_item_title'=> 'დამატება',
                //'Icon'              => 'Icon',
                'edit_item_title' 	=> 'Edit Menu Item',
                'options'			=> ['edit', 'sort', 'status','delete']
            ], 'Langs' => $supportedLocalekeys,'Locale' => $locale]);

    }
}
