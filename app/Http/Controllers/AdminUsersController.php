<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersPostRequest;
use App\Repositories\BaseRepository;
use App\Services\AdminUsersServices;


class AdminUsersController extends BaseController
{

    private $adminUsersServices;
    private $baseRepository;
    public function __construct(AdminUsersServices $adminUsersServices,BaseRepository $baseRepository)
    {
        $this->middleware('auth:admin');
        $this->adminUsersServices = $adminUsersServices;
        $this->baseRepository = $baseRepository;
    }


    public function Users()
    {
        $Users = $this->adminUsersServices->getUsers();
        return view('admin.users.index', ['Users' => $Users]);
    }

    public function addUsers()
    {
        $Roles = $this->adminUsersServices->getRoles();
        return view('admin.users.add',['Roles' => $Roles]);
    }

    public function AddUsersPost(UsersPostRequest $request)
    {
        $this->adminUsersServices->Create($request);
    }

    public function UsersById(int $PostID){

        $item = $this->adminUsersServices->UsersById($PostID);
        $Roles = $this->adminUsersServices->getRoles();
        return view('admin.users.edit', ['item' => $item, 'Roles' => $Roles]);
    }

    public function EditUsersPost(UsersPostRequest $request)
    {
        $this->adminUsersServices->Update($request);
    }

}
