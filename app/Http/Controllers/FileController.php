<?php

namespace App\Http\Controllers;
use App\Services\FileServices;
use Illuminate\Http\Request;

class FileController extends BaseController
{
    private $fileServices;
    public function __construct(FileServices $fileServices)
    {
        $this->middleware('auth:admin');
        $this->fileServices = $fileServices;
    }

    public function UploadFiles(Request $request) {
        return $this->fileServices->UploadFiles($request);
        }
}
