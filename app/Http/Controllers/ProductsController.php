<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductPostRequest;
use App\Repositories\BaseRepository;
use App\Services\ProductServices;
use App\Transformers\AddressTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends BaseController
{

    private $productServices;
    private $baseRepository;
    public function __construct(ProductServices $productServices,BaseRepository $baseRepository)
    {
        //$this->middleware('auth:admin');
        $this->productServices = $productServices;
        $this->baseRepository = $baseRepository;
    }

    public function index(Request $request, $Search = null){
        $meta = $this->baseRepository->GetMeta(3);
        $DealTypes = $this->baseRepository->GetDeals();
        $PropertyTypes = $this->baseRepository->GetProperties();
        $Location = $this->productServices->GetRegions();
        $MinPrice = $this->productServices->MinPrice();
        $MaxPrice = $this->productServices->MaxPrice();
        $Word = $request->Word;
        $SelectedDeal = $request->Deal;
        $SelectedProperty = $request->Property;
        $SelectedLocation = $request->Location;
        $SelectedSpaceFrom = $request->SpaceFrom;
        $SelectedSpaceTo = $request->SpaceTo;
        if ($request->has('Word')){
            $Products = $this->productServices->SearchResults($request);
        } else {
            $Products = $this->productServices->Front();
        }
        return view('front.products.index', ['Products' => $Products, 'Meta' => $meta, 'DealTypes' => $DealTypes, 'PropertyTypes' => $PropertyTypes, 'Location' => $Location, 'MinPrice' => $MinPrice, 'MaxPrice' => $MaxPrice,
        'Word' => $Word, 'SelectedDeal' => $SelectedDeal, 'SelectedProperty' => $SelectedProperty, 'SelectedLocation' => $SelectedLocation, 'SelectedSpaceFrom' => $SelectedSpaceFrom, 'SelectedSpaceTo' => $SelectedSpaceTo
        ]);
    }

    public function Products()
    {
        $Deal = $this->baseRepository->GetDeals();
        $Property = $this->baseRepository->GetProperties();
        return view('admin.products.index', ['Deal' => $Deal, 'Property' => $Property]);
    }

    public function ProductsAjax(Request $request)
    {
        return $this->productServices->ProductsAjaxFilter($request);

    }
    public function Show(int $PostID){
        $this->productServices->IncreaseViews($PostID);
        $Products = $this->productServices->ShowF($PostID);
        $MainCats = $this->productServices->MainCats($Products->id);
        $ProductCats = $this->productServices->ProductCats($Products->id);
        $SameProducts = $this->productServices->ShowSameProducts($Products->id,$Products->type_id,$Products->urban_id,$Products->deal_id,$Products->price_type, $Products->district_id,6);
        $Services = $this->baseRepository->ServicesByLimit(5);
        $Blog = $this->baseRepository->BlogByLimit(3);
        return view('front.products.detail', ['item' => $Products, 'MainCats' => $MainCats, 'ProductCats' => $ProductCats, 'SameProducts' => $SameProducts, 'Services' => $Services, 'Blog' => $Blog]);
    }

    public function addProducts(int $CatId)
    {
        $Attrs = $this->productServices->GetAttrs($CatId);
        $Deal = $this->baseRepository->GetDeals();
        return view('admin.products.add',['Attrs' => $Attrs, 'Type_id' => $CatId, 'Deal' => $Deal]);
    }

    public function LoadLocations(Request $request){
        if ($request->has('id')){
            $address = DB::table($request->type)->where('title_ka','LIKE',"%{$request->search}%");
            if ($request->has('id') && !is_null($request->id)) {
                $address->where($request->parent.'_id', $request->id);}
            $address = $address->paginate(30);
            $response['items'] = fractal($address, new AddressTransformer())->toArray();;
            $response['total_count'] = $address->total();
            return response()->json($response);
        } else {
            $response['items']['data'] = [];
            $response['total_count'] = 0;
            return response()->json($response);
        }

    }

    public function AddProductsPost(ProductPostRequest $request)
    {
        $this->productServices->Create($request);
    }

    public function ProductsById(int $PostID){

        $item = $this->productServices->ShowB($PostID);
        $Attrs = $this->productServices->GetAttrs($item->type_id);
        $ProductsAttrs = $this->productServices->GetSelectedAttrs($item->id);
        $ProductRegion = $this->productServices->GetSelectedRegion($item->region_id);
        $ProductDistrict = $this->productServices->GetSelectedDistrict($item->district_id);
        $ProductUrban = $this->productServices->GetSelectedUrban($item->urban_id);
        $ProductStreet = $this->productServices->GetSelectedStreet($item->street_id);
        $Deal = $this->baseRepository->GetDeals();
        $Files = $this->baseRepository->GetFiles($PostID, 'products');
        return view('admin.products.edit', ['item' => $item,'Files' => $Files,
            'Deal'=> $Deal, 'Attrs' => $Attrs, 'ProductsAttrs' => $ProductsAttrs,
            'ProductRegion'=> $ProductRegion,'ProductDistrict'=> $ProductDistrict,'ProductUrban'=> $ProductUrban,'ProductStreet'=> $ProductStreet
            ]);
    }

    public function EditProductsPost(ProductPostRequest $request)
    {
        $this->productServices->Update($request);
    }

}
