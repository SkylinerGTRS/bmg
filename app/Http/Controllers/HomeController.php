<?php

namespace App\Http\Controllers;

use App\Repositories\BaseRepository;


class HomeController extends Controller
{
    private $baseRepository;
    public function __construct(BaseRepository $baseRepository)
    {
        $this->baseRepository = $baseRepository;
    }
    public function index()
    {
        $Meta = $this->baseRepository->getMeta(1);
        $Services = $this->baseRepository->ServicesByLimit(5);
        $ExcStatements = $this->baseRepository->ExcStatementsByLimit(8);
        $VipStatements = $this->baseRepository->VipStatementsByLimit(8);
        $Properties = $this->baseRepository->getPropertiesWithStatements();
        $ID = [];
        foreach ($Properties as $property){
            foreach ($property->Statements as $st)
            array_push($ID, $st->id);
        }
        $NewStatements = $this->baseRepository->NewStatements(10,$ID);
        $Blog = $this->baseRepository->BlogByLimit(3);
        return view('front.home.index',['Meta' => $Meta, 'Blog' => $Blog,
            'NewStatements' => $NewStatements,'ExcStatements' =>  $ExcStatements, 'VipStatements' => $VipStatements, 'Services' => $Services, 'Properties' => $Properties]);
    }


}
