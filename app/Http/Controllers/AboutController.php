<?php

namespace App\Http\Controllers;

use App\Http\Requests\AboutPostRequest;
use App\Repositories\BaseRepository;
use App\Services\AboutServices;

class AboutController extends BaseController
{

    private $aboutServices;
    private $baseRepository;
    public function __construct(AboutServices $aboutServices,BaseRepository $baseRepository)
    {
        //$this->middleware('auth:admin');
        $this->aboutServices = $aboutServices;
        $this->baseRepository = $baseRepository;
    }

    public function index(){
        $meta = $this->baseRepository->GetMeta(4);
        $AboutText = $this->aboutServices->AboutMain();
        $About = $this->aboutServices->Front($AboutText->id);
        return view('front.about.index', ['About' => $About, 'Meta' => $meta , 'MainAbout' => $AboutText]);
    }
    public function About()
    {
        $About = $this->aboutServices->Back();
        return view('admin.about.index', ['About' => $About]);
    }
    public function Show(int $PostID){
        $About = $this->aboutServices->ShowF($PostID);
        return view('front.about.detail', ['About' => $About]);
    }

    public function addAbout()
    {
        return view('admin.about.add');
    }

    public function AddAboutPost(AboutPostRequest $request)
    {
        $this->aboutServices->Create($request);
    }

    public function AboutById($PostID){

        $item = $this->aboutServices->ShowB($PostID);
        $Files = $this->baseRepository->GetFiles($PostID, 'about');
        return view('admin.about.edit', ['item' => $item,'Files' => $Files]);
    }

    public function EditAboutPost(AboutPostRequest $request)
    {
        $this->aboutServices->Update($request);
    }

}
