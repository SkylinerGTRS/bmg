<?php

namespace App\Helpers;
use App\Models\Files;
use App\Models\Attrs;
use App\Transformers\RegionsTransformer;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Helpers
{
    public static function UploadImages($File,$ID,$Route){
        //check if image is file instance
        if(!is_array($File)) {
            $extension = pathinfo(public_path('/tmp/'.$File), PATHINFO_EXTENSION);
            if (in_array($extension, unserialize(config('constants.IMG')))) {
                $largedir = public_path() . config('constants.UPLOADS.' . $Route . '.LARGE_DIR');
                $thumbsdir = public_path(config('constants.UPLOADS.' . $Route . '.THUMBS_DIR'));
                //check if the directory exists
                if (!File::isDirectory($largedir)) {
                    //make the directory because it doesn't exists
                    File::makeDirectory($largedir, 0755, true);
                }
                if (!File::isDirectory($thumbsdir)) {
                    File::makeDirectory($thumbsdir, 0755, true);
                }
                $L = File::move(
                    public_path() . '/tmp/large_' . $File,
                    public_path() . config('constants.UPLOADS.' . $Route . '.LARGE_DIR') . $File);
                $T = File::move(
                    public_path() . '/tmp/thumb_' . $File,
                    public_path() . config('constants.UPLOADS.' . $Route . '.THUMBS_DIR') . $File);
                //if not file, than just save to db
                if ($L && $T) {
                    $file = new Files;
                    //assigning
                    $file->route_id = $ID;
                    $file->file_size = File::size(public_path().config('constants.UPLOADS.'.$Route.'.LARGE_DIR').$File);
                    $file->name = $File;
                    $file->route_name = $Route;
                    //saving records
                    if ($file->save()){
                        return response()->json(['StatusCode' => 1,
                            'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']);
                    }

                } else {
                    return response()->json(['StatusCode' => 0,
                        'StatusMessage' => 'ვერ მოხერხდა ტმპ ფოლდერიდან ფაილების გადატანა']);
                }
            } elseif ($extension == 'svg') {
                $svgdir = public_path(config('constants.UPLOADS.' . $Route . '.SVG_DIR'));
                if (!File::isDirectory($svgdir)) {
                    //make the directory because it doesn't exists
                    File::makeDirectory($svgdir, 0755, true);
                }
                $SVG = File::move(
                    public_path() . '/tmp/' . $File,
                    public_path() . config('constants.UPLOADS.' . $Route . '.SVG_DIR') . $File);
                if ($SVG){
                    $file = new Files;
                    //assigning
                    $file->route_id = $ID;
                    $file->file_size = File::size(public_path().config('constants.UPLOADS.'.$Route.'.SVG_DIR').$File);
                    $file->name = $File;
                    $file->route_name = $Route;
                    //saving records
                    if ($file->save()){
                        return response()->json(['StatusCode' => 1,
                            'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']);
                    }
                }else {
                    return response()->json(['StatusCode' => 0,
                        'StatusMessage' => 'ვერ მოხერხდა ტმპ ფოლდერიდან ფაილების გადატანა']);
                }

            }


        } elseif (is_array($File)){
            $file = new Files;
            //assigning
            $file->route_id = $ID;
            $file->file_size = $File['size'];
            $file->name = $File['name'];
            $file->route_name = $Route;
            //saving records
            if ($file->save()){
                return response()->json(['StatusCode' => 1,
                    'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']);

            }
        }

    }

    public static function FUrl($string) {
        return str_replace(array('+', '-', '@', '>', '<', '(', ')', '~', '*', '?', '%', ',', '.', ':', 'Ø', '/', ';', '&', '#', '!', '$', '^', '*', '_', '=', '|', ' ', '"'), '-', $string);
    }

    public static function ProceedUpload(Request $request, $Route, $Id){
        if ($request->has('File')) {
            if (Helpers::RemoveFiles($Route, $Id)) {
                foreach ($request->File as $image){
                    Helpers::UploadImages($image,$Id,$Route);
                }
            } else {
                throw new HttpResponseException(response()
                    ->json(['StatusCode' => 0,
                        'StatusMessage' => 'დაფიქსირდა შეცდომა ფაილების წაშლისას!']));
            }

        } elseif($request->file('File') == null || !$request->file('File')){
            return Helpers::RemoveFiles($Route,$Id);
        }
    }

    public static function UploadCategoryIcons($Icon)
    {

        $imageName = time().'.'.$Icon->getClientOriginalExtension();
        $Icon->move(public_path('images'), $imageName);
        return $imageName;
    }

    public static function GenerateTrans(Request $request, $param){
        $item=[];
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        foreach ($supportedLocalekeys as $localekey) {
            $item[$localekey] = $request->input($param. $localekey);
        }
        return $item;
    }

    public static function RemoveFiles($Route,$ID)
    {
        $FileCount = Files::where('route_id', $ID)
            ->where('route_name', $Route)->count();
        if ($FileCount == 0){
            return true;
        } else {
            return Files::where('route_id', $ID)
                ->where('route_name', $Route)
                ->delete();
        }

    }

    public static function TraverseCats($List = array())
    {
        $Cats = array();
        foreach($List AS $CatID => $CatData) {
            if(intval($CatData['parent_id']) == 0) {
                $Cats[$CatData['cat_id']] = $CatData;
                continue;
            }

            if(isset($Cats[$CatData['parent_id']])) {
                if(!isset($Cats[$CatData['parent_id']]['children'])){
                    $Cats[$CatData['parent_id']]['children'] = [];
                }
                $Cats[$CatData['parent_id']]['children'][$CatData['cat_id']] = $CatData;
            }
        }
        return $Cats;
    }

    public static function Save(Request $request, $Instance, string $Route)
    {
        if ($Instance->save()) {
            Attrs::where('pr_id',$Instance->id)->delete();
            if ($request->has('Attr')){
                foreach ($request->input('Attr') as $key => $value){
                    if (!is_null($value)){
                        $Attrs = new Attrs;
                        $Attrs->pr_id = $Instance->id;
                        $Attrs->attr_id = $key;
                        $Attrs->value = $value;
                        $Attrs->saveOrFail();
                    }

                }
            }
            if (Helpers::ProceedUpload($request, $Route, $Instance->id)){
                throw new HttpResponseException(response()
                    ->json(['StatusCode' => 1,
                        'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']));
            }
            throw new HttpResponseException(response()
                ->json(['StatusCode' => 1,
                    'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']));
        } else {
            throw new HttpResponseException(response()
                ->json(['StatusCode' => 0,
                    'StatusMessage' => 'დაფიქსირდა შეცდომა!']));
        }
    }

    public static function LoadLocatios(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        // Total records
        $totalRecords = DB::table($request->PostTable)->count();

        // Fetch records
        $records = DB::table($request->PostTable)->where('status_id','!=',2)->orderBy('id','desc');
        if ($request->has('type_id') && !is_null($request->type_id)){
            if ($request->PostTable == 'districts') {
                $records->where('regions_id', $request->type_id);
            } else if ($request->PostTable == 'urban'){
                $records->where('districts_id', $request->type_id);
            } else if ($request->PostTable == 'streets'){
                $records->where('urban_id', $request->type_id);
            }
        }

        if ($request->has('title') && !is_null($request->title)){
            $records->where('title_ka','like', '%'.$request->title.'%');
        }

        $count = $records->count();
        $records->skip($start)->take($rowperpage);

        if ($totalRecords == $count){
            $totalRecordswithFilter = $totalRecords;
        } else {
            $totalRecordswithFilter = $count;
        }


        $response = fractal($records->get(), new RegionsTransformer($request->PostTable))->toArray();
        $response['draw'] = intval($draw);
        $response['recordsTotal'] = $totalRecords;
        $response['recordsFiltered'] = $totalRecordswithFilter;

        return response()->json($response);
    }
}
