<?php

namespace App\Providers;

use App\Models\Contact;
use App\Models\Orders;
use App\Models\PropertyCategories;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        localization()->setLocale(app()->getLocale());
        $supportedLocales = localization()->getSupportedLocales();
        $supportedLocalekeys = localization()->getSupportedLocalesKeys();
        $locale = localization()->getCurrentLocale();
        $checked = Orders::where('checked',1)->count();
        $notchecked = Orders::where('checked',0)->count();
        $contact = Contact::find(1);
        $MainCats = PropertyCategories::where('status_id','=',1)
            ->orderBy('parent_id')
            ->orderBy('sort_order','asc')->get();
        Schema::defaultStringLength(191);
        view()->share('supportedLocales', $supportedLocales);
        view()->share('Locale', $locale);
        view()->share('Langs', $supportedLocalekeys);
        view()->share('Contact', $contact);
        view()->share('MainCats', $MainCats);
        view()->share('Checked', $checked);
        view()->share('NotChecked', $notchecked);
        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }
}
