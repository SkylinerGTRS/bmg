<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected static function boot()
    {
        parent::boot();
    }
    protected $primaryKey = 'id';
    protected $table = 'regions';

}
