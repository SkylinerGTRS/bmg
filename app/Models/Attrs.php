<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int pr_id
 * @property int attr_id
 * @property  int|string value
 */
class Attrs extends Model
{
    protected static function boot()
    {
        parent::boot();
    }
    protected $primaryKey = 'id';
    protected $table = 'product_attrs';

    protected $fillable = ['pr_id','attr_id','value'];

    public function Category(){
        return $this->hasOne( 'App\Models\Categories', 'cat_id', 'attr_id')->where('is_main', 0);
    }

    public function MainCat(){
        return $this->hasOne( 'App\Models\Categories', 'cat_id', 'attr_id')->where('is_main', 1);
    }
}
