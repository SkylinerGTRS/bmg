<?php

namespace App\Models;

use App\Scopes\StatusScopes\DeletedScope;
use Arcanedev\Localization\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int|mixed sort_order
 * @property array|string|null parent_id
 * @property int cat_id
 */
class DealCategories extends Model
{
    use HasTranslations;
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new DeletedScope);
    }

    public function getTranslatableAttributes()
    {
        return ['title'];
    }

    public function scopeActive($query)
    {
        return $query->where('status_id', 1);
    }

    protected $primaryKey = 'cat_id';
    protected $table = 'dealcategories';
    protected $fillable = ['sort_order','title'];
}
