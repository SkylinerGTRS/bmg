<?php

namespace App\Models;

use App\Scopes\StatusScopes\DeletedScope;
use Arcanedev\Localization\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int|mixed sort_order
 * @property array|string|null parent_id
 * @property int cat_id
 */
class PropertyCategories extends Model
{
    use HasTranslations;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new DeletedScope);
    }

    public function Categories()
    {
        return $this->hasMany('App\Models\Categories','type_id','cat_id');
    }

    public function Statements()
    {
        return $this->hasMany('App\Models\Products','type_id','cat_id')->where('exc','!=', 1)
            ->where('vip','!=',1);
    }

    public function getTranslatableAttributes()
    {
        return ['title'];
    }

    public function scopeActive($query)
    {
        return $query->where('status_id', 1);
    }

    protected $primaryKey = 'cat_id';
    protected $table = 'propertycategories';
    protected $fillable = ['sort_order','title'];
}
