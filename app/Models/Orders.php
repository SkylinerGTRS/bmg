<?php

namespace App\Models;

use App\Scopes\StatusScopes\ActiveScope;
use App\Scopes\StatusScopes\DeletedScope;
use Arcanedev\Localization\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * @property integer id
 */
class Orders extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new DeletedScope);
    }
    protected $primaryKey = 'id';
    protected $table = 'orders';

    public function Images(){
        return $this->hasMany( 'App\Models\Files', 'route_id', 'id')->where('route_name', 'orders')->orderBy('id', 'asc');
    }
}
