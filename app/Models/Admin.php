<?php

namespace App\Models;


use App\Scopes\StatusScopes\DeletedScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    public function getRoleName(): Collection
    {
        return $this->roles;
    }
    use HasFactory;
    protected $guard = 'admin';
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new DeletedScope);
    }
    protected $fillable = [
        'name','username', 'password',
    ];

    protected $hidden = [
        'password'
    ];

}
