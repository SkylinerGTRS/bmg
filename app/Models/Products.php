<?php

namespace App\Models;

use App\Scopes\StatusScopes\ActiveScope;
use App\Scopes\StatusScopes\DeletedScope;
use App\Support\Database\CacheQueryBuilder;
use Arcanedev\Localization\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 * @property array|string|null theme_id
 * @property array|string|null cat_id
 * @property array|string|null direction_id
 */
class Products extends Model
{
    use HasTranslations;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new DeletedScope);
        static::addGlobalScope(new ActiveScope);
    }
    protected $primaryKey = 'id';
    protected $table = 'products';
    protected $fillable = [
        'title','created_at'
    ];
    public function MainImage()
    {
        return $this->hasOne('App\Models\Files', 'route_id', 'id')->where('route_name', 'products')->orderBy('id', 'asc');
    }

    public function Images(){
        return $this->hasMany( 'App\Models\Files', 'route_id', 'id')->where('route_name', 'products')->orderBy('id', 'asc');
    }

    public function Attrs(){
        return $this->hasMany( 'App\Models\Attrs', 'pr_id', 'id');
    }

    public function Deal(){
        return $this->hasOne( 'App\Models\DealCategories', 'cat_id', 'deal_id' );
    }

    public function Property(){
        return $this->hasOne( 'App\Models\PropertyCategories', 'cat_id', 'type_id' );
    }

    public function Urban(){
        return $this->hasOne( 'App\Models\Urban', 'id', 'urban_id' );
    }

    public function getTranslatableAttributes()
    {
        return ['title','descr','address'];
    }
}
