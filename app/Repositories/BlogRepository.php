<?php

namespace App\Repositories;

use App\Models\Blog;
use App\Helpers\Helpers;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use App\Scopes\StatusScopes\ActiveScope;
use Illuminate\Http\Request;

class BlogRepository implements BlogRepositoryInterface
{

    public function Front(int $ID = null, int $paginate = null)
    {
        if (!is_null($ID)) {
            return Blog::with('MainImage')->where('id','!=',$ID)->orderBy('created_at', 'desc')->paginate($paginate);
        } else {
            return Blog::with('MainImage')->orderBy('created_at', 'desc')->paginate($paginate);
        }

    }

    public function Back()
    {
        return Blog::withoutGlobalScope(ActiveScope::class)->orderBy('created_at', 'asc')->get();
    }

    public function ShowF(int $Id)
    {
        return Blog::with('MainImage')->orderBy('created_at', 'desc')->findOrFail($Id);
    }

    public function ShowB(int $Id)
    {
        return Blog::withoutGlobalScope(ActiveScope::class)->find($Id);
    }

    public function Create(Request $request)
    {
        $Blog = new Blog;
        $Blog->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Blog->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Blog,'blog');
    }

    public function Update(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new Blog;
        $Blog = $Item::withoutGlobalScope(ActiveScope::class)->find($ID);
        $Blog->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Blog->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Blog,'blog');
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return Blog::with('MainImage')
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $id)->orderByRaw('updated_at DESC')
            ->Limit($limit)
            ->get();
    }

    public function MainBlog()
    {
        return Blog::where('is_main',1)->first();
    }
}
