<?php

namespace App\Repositories;

use App\Helpers\Helpers;
use App\Models\Meta;
use App\Repositories\Interfaces\MetaRepositoryInterface;
use App\Scopes\StatusScopes\ActiveScope;
use Illuminate\Http\Request;

class MetaRepository implements MetaRepositoryInterface
{

    public function Back()
    {
        return Meta::withoutGlobalScope(ActiveScope::class)->orderBy('created_at','asc')->get();
    }

    public function Create(Request $request)
    {
        $Meta = new Meta;
        $Meta->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Meta->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Meta,'meta');
    }

    public function Update(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new Meta;
        $Meta = $Item::withoutGlobalScope(ActiveScope::class)->find($ID);
        $Meta->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Meta->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Meta,'meta');
    }

    public function Show(int $Id)
    {
        return Meta::withoutGlobalScope(ActiveScope::class)->find($Id);
    }
}
