<?php

namespace App\Repositories;

use App\Models\Attrs;
use App\Models\Categories;
use App\Models\Products;
use App\Helpers\Helpers;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Scopes\StatusScopes\ActiveScope;
use App\Transformers\ProductsTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductRepository implements ProductRepositoryInterface
{

    public function Front()
    {
        return Products::with(['MainImage','Urban','Property'])->orderBy('created_at', 'desc')->paginate(12);
    }

    public function Back()
    {
        return Products::withoutGlobalScope(ActiveScope::class)->orderBy('created_at', 'asc')->get();
    }

    public function ShowF(int $Id)
    {
        return Products::with(['MainImage','Urban','Property'])->orderBy('created_at', 'desc')->findOrFail($Id);
    }

    public function ShowB(int $Id)
    {
        return Products::withoutGlobalScope(ActiveScope::class)->with('Attrs')->find($Id);
    }

    public function GetSelectedAttrs(int $Id) {
        $Attrs = (Attrs::where('pr_id',$Id)->get())->toArray();
        $array = [];
        foreach ($Attrs as $item){
            $array[$item['attr_id']] = $item;
        }
        return $array;
    }

    public function Create(Request $request)
    {
        $Products = new Products;
        $Products->type_id = $request->input('TypeId');
        $Products->region_id = $request->input('regionId');
        $Products->district_id = $request->input('districtId');
        $Products->urban_id = $request->input('urbanId');
        $Products->street_id = $request->input('streetId');
        $Products->deal_id = $request->input('DealId');
        $Products->price_type = $request->input('price_type');
        $Products->space = $request->input('space');
        $Products->price_usd = $request->input('price_usd');
        $Products->price_gel = $request->input('price_gel');
        $Products->business = $request->input('business');
        $Products->vip = $request->input('Vip');
        $Products->exc = $request->input('Exc');
        $Products->setTranslations('address', Helpers::GenerateTrans($request, 'Address-'));
        $Products->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Products->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Products,'products');
    }

    public function Update(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new Products;
        $Products = $Item::withoutGlobalScope(ActiveScope::class)->find($ID);
        $Products->type_id = $request->input('TypeId');
        $Products->region_id = $request->input('regionId');
        $Products->district_id = $request->input('districtId');
        $Products->urban_id = $request->input('urbanId');
        $Products->street_id = $request->input('streetId');
        $Products->deal_id = $request->input('DealId');
        $Products->price_type = $request->input('price_type');
        $Products->space = $request->input('space');
        $Products->price_usd = $request->input('price_usd');
        $Products->price_gel = $request->input('price_gel');
        $Products->business = $request->input('business',0);
        $Products->vip = $request->input('Vip',0);
        $Products->exc = $request->input('Exc',0);
        $Products->setTranslations('address', Helpers::GenerateTrans($request, 'Address-'));
        $Products->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Products->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Products,'products');
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return Products::with('MainImage')
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $id)->orderByRaw('updated_at DESC')
            ->Limit($limit)
            ->get();
    }

    public function GetAttrs(int $CatId)
    {
        return Categories::with('children')->orderBy('parent_id')
            ->where('type_id',$CatId)
            ->orderBy('options','asc')->get();
    }

    public function GetSelectedRegion(int $region_id)
    {
        return DB::table('regions')->find($region_id);
    }

    public function GetSelectedDistrict(int $district_id)
    {
        return DB::table('districts')->find($district_id);
    }

    public function GetSelectedUrban(int $urban_id)
    {
        return DB::table('urban')->find($urban_id);
    }

    public function GetSelectedStreet(int $street_id)
    {
        return DB::table('streets')->find($street_id);
    }

    public function ProductsAjaxFilter(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        // Total records
        $totalRecords = Products::withoutGlobalScope(ActiveScope::class)->count();

        // Fetch records
        $records = Products::withoutGlobalScope(ActiveScope::class)->with(['Deal','Property'])->orderBy('id','desc');
        if ($request->has('searchByDeal') && !is_null($request->searchByDeal)){
            $records->where('deal_id', $request->searchByDeal);
        }

        if ($request->has('searchByProperty') && !is_null($request->searchByProperty)){
            $records->where('type_id', $request->searchByProperty);
        }

        if ($request->has('title') && !is_null($request->title)){
            $records->where('title->ka', 'like', '%' . $request->title . '%');
        }

        if ($request->has('id') && !is_null($request->id)){
            $records->where('id', (int)$request->id);
        }

        if ($request->has('price_from') && !is_null($request->price_from) && is_null($request->price_to)){
            $records->where('price_gel', '>' , (int)$request->price_from);
        }

        if ($request->has('price_to') && !is_null($request->price_to) && is_null($request->price_from)){
            $records->where('price_gel','<', (int) $request->price_to);
        }

        if (!is_null($request->price_from) && !is_null($request->price_to)){
            $records->whereBetween('price_gel',[(int) $request->price_from, (int) $request->price_to]);
        }
        $count = $records->count();

        $records->skip($start)->take($rowperpage);

        if ($totalRecords == $count){
            $totalRecordswithFilter = $totalRecords;
        } else {
            $totalRecordswithFilter = $count;
        }


        $response = fractal($records->get(), new ProductsTransformer())->toArray();
        $response['draw'] = intval($draw);
        $response['recordsTotal'] = $totalRecords;
        $response['recordsFiltered'] = $totalRecordswithFilter;

        return response()->json($response);
    }

    public function MainCats(int $id)
    {
        return Attrs::with('MainCat')->whereHas('MainCat')->where('pr_id', $id)->get();
    }

    public function ProductCats(int $id)
    {
        return Attrs::with('Category')->whereHas('Category')->where('pr_id',$id)->get();
    }

    public function ShowSameProducts(int $id, int $type_id, int $urban_id, int $deal_id, int $price_type, int $district_id, int $limit)
    {
        return Products::with(['MainImage','Urban','Property'])
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $id)
            ->orWhere('type_id',$type_id)
            ->orWhere('urban_id',$urban_id)
            ->orWhere('deal_id',$deal_id)
            ->orWhere('price_type',$price_type)
            ->orWhere('district_id',$district_id)
            ->Limit($limit)
            ->get();
    }

    public function IncreaseViews(int $Id)
    {
        if (Cookie::get('Product-'.$Id) != $Id){
            Cookie::queue('Product-'.$Id, $Id, 1440);
            $Product = Products::find($Id);
            $Product->timestamps = false;
            $Product->increment('views');
            $Product->save();
        } else {
            Cookie::queue('Product-'.$Id, $Id, 1440);
        }
    }

    public function GetRegions()
    {
       return DB::table('regions')->get();
    }

    public function MinPrice()
    {
        return Products::min('price_gel');
    }

    public function MaxPrice()
    {
        return Products::max('price_gel');
    }

    public function SearchResults(Request $request)
    {

        $records = Products::with(['Deal','Property'])->orderBy('id','desc');
        if ($request->has('Word') && !is_null($request->Word)){
            $records->where('title->ka', 'like', '%' . $request->Word . '%')
                ->orWhere('title->en', 'like', '%' . $request->Word . '%')
                ->orWhere('title->ru', 'like', '%' . $request->Word . '%');
        }
        if ($request->has('Deal') && !is_null($request->Deal)){
            $records->whereIn('deal_id', $request->Deal);
        }
        if ($request->has('Property') && !is_null($request->Property)){
            $records->whereIn('type_id', $request->Property);
        }

        if ($request->has('Location') && !is_null($request->Location)){
            $records->whereIn('region_id', $request->Location);
        }

        if ($request->has('SpaceFrom') && !is_null($request->SpaceFrom) && is_null($request->SpaceTo)){
            $records->where('space', '>' , (int)$request->SpaceFrom);
        }

        if ($request->has('SpaceTo') && !is_null($request->SpaceTo) && is_null($request->SpaceFrom)){
            $records->where('space','<', (int) $request->SpaceTo);
        }

        if (!is_null($request->SpaceTo) && !is_null($request->SpaceFrom)){
            $records->whereBetween('space',[(int) $request->SpaceFrom, (int) $request->SpaceTo]);
        }

        if (!is_null($request->SelectedFromVal) && !is_null($request->SelectedToVal)){
            $records->whereBetween('price_gel',[(int) $request->SelectedFromVal, (int) $request->SelectedToVal]);
        }

        return $records->paginate(12);
    }
}
