<?php

namespace App\Repositories;

use App\Helpers\Helpers;

use App\Models\Orders;
use App\Repositories\Interfaces\OrdersRepositoryInterface;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class OrderRepository implements OrdersRepositoryInterface
{

    public function AddStatementPost(Request $request)
    {
        $Orders = new Orders;
        $Orders->name = $request->input('Name');
        $Orders->currency = $request->input('currency');
        $Orders->lastname = $request->input('Lastname');
        $Orders->phone = $request->input('Phone');
        $Orders->email = $request->input('Email');
        $Orders->code = $request->input('Code');
        $Orders->price = $request->input('Price');
        $Orders->message = $request->input('Message');
        Helpers::Save($request, $Orders,'orders');
    }

    public function NotCheckedOrders()
    {
        return Orders::where('checked', 0)->get();
    }

    public function OrdersById(int $ID)
    {
        return Orders::findorfail($ID);
    }

    public function EditOrdersPost(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new Orders;
        $Order = $Item::find($ID);
        $Order->checked = 1;
        if ($Order->save()){
            throw new HttpResponseException(response()
                ->json(['StatusCode' => 1,
                    'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']));
        } else {
            throw new HttpResponseException(response()
                ->json(['StatusCode' => 0,
                    'StatusMessage' => 'დაფიქსირდა შეცდომა!']));
        }
    }

    public function CheckedOrders()
    {
        return Orders::where('checked', 1)->get();
    }
}
