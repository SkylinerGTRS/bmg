<?php

namespace App\Repositories;

use App\Models\Blog;
use App\Models\Files;
use App\Models\Meta;
use App\Models\DealCategories;
use App\Models\Products;
use App\Models\PropertyCategories;
use App\Models\Services;
use App\Repositories\Interfaces\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{

    public function GetFiles(int $Id, string $Route)
    {
        return Files::where('route_id', $Id)
            ->where('route_name', $Route)
            ->get()->toJson();
    }

    public function GetMeta(int $Id)
    {
        return Meta::where('type_id', '=', $Id)->first();
    }

    public function GetDeals()
    {
        return DealCategories::all();
    }

    public function GetProperties()
    {
        return PropertyCategories::all();
    }

    public function ServicesByLimit(int $int, int $ID = null)
    {
        if (!is_null($ID)){
            return Services::with(['MainImage'])->limit($int)->where('id','!=',$ID)->orderBy('id', 'desc')->get();
        } else {
            return Services::with(['MainImage'])->limit($int)->orderBy('id', 'desc')->get();
        }

    }

    public function BlogByLimit(int $int, int $ID = null)
    {
        if (!is_null($ID)){
            return Blog::with(['MainImage'])->limit($int)->where('id','!=',$ID)->orderBy('id', 'desc')->get();
        } else {
            return Blog::with(['MainImage'])->limit($int)->orderBy('id', 'desc')->get();
        }

    }

    public function ExcStatementsByLimit(int $int)
    {
        return Products::with(['Urban', 'MainImage' , 'Property'])->where('exc','=', 1)->limit($int)->orderBy('id', 'desc')->get();
    }

    public function NewStatements(int $int, array $ID)
    {
        return Products::with(['Urban','MainImage'])->where('exc','!=', 1)->where('vip','!=',1)->whereNotIn('id',$ID)->orderBy('id', 'desc')->limit($int)->get();
    }

    public function VipStatementsByLimit(int $int)
    {
        return Products::with(['Urban', 'MainImage' , 'Property'])->where('vip','=', 1)->limit($int)->orderBy('id', 'desc')->get();
    }

    public function getPropertiesWithStatements()
    {
        return PropertyCategories::whereHas('Statements')->with(['Statements' => function ($query) {
            $query->latest()->limit(8);
        },'Statements.Urban', 'Statements.MainImage' , 'Statements.Property'])->get();
    }
}
