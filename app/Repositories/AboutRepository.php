<?php

namespace App\Repositories;

use App\Models\About;
use App\Helpers\Helpers;
use App\Repositories\Interfaces\AboutRepositoryInterface;
use App\Scopes\StatusScopes\ActiveScope;
use Illuminate\Http\Request;

class AboutRepository implements AboutRepositoryInterface
{

    public function Front(int $ID = null)
    {
        if (!is_null($ID)){
            return About::with('MainImage')->where('id','!=',$ID)->orderBy('created_at', 'desc')->get();
        } else {
            return About::with('MainImage')->orderBy('created_at', 'desc')->get();
        }
    }
    public function AboutMain()
    {
        return About::find(1);
    }

    public function Back()
    {
        return About::withoutGlobalScope(ActiveScope::class)->orderBy('created_at', 'asc')->get();
    }

    public function ShowF(int $Id)
    {
        return About::with('MainImage')->orderBy('created_at', 'desc')->findOrFail($Id);
    }

    public function ShowB(int $Id)
    {
        return About::withoutGlobalScope(ActiveScope::class)->find($Id);
    }

    public function Create(Request $request)
    {
        $About = new About;
        $About->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $About->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        $About->setTranslations('position', Helpers::GenerateTrans($request, 'Position-'));
        Helpers::Save($request, $About,'about');
    }

    public function Update(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new About;
        $About = $Item::withoutGlobalScope(ActiveScope::class)->find($ID);
        $About->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $About->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        $About->setTranslations('position', Helpers::GenerateTrans($request, 'Position-'));
        Helpers::Save($request, $About,'about');
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return About::with('MainImage')
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $id)->orderByRaw('updated_at DESC')
            ->Limit($limit)
            ->get();
    }
}
