<?php

namespace App\Repositories;

use App\Models\Admin;
use App\Repositories\Interfaces\AdminUsersRepositoryInterface;
use Illuminate\Http\Exceptions\HttpResponseException;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUsersRepository implements AdminUsersRepositoryInterface
{

    public function getUsers()
    {
        return Admin::where('status_id', '!=', 3)->get();
    }


    public function UsersById(int $Id)
    {
        return Admin::find($Id);
    }

    public function Create(Request $request)
    {
        $user = new Admin();
        $user->name = $request->input('Name');
        $user->username = $request->input('username');
        $user->password = Hash::make($request->input('Password','NeverForget'));

        if ($user->save()){
            $user->assignRole($request->input('RoleName'));
            throw new HttpResponseException(response()
                ->json(['StatusCode' => 1, 'StatusMessage' => 'ოპერაცია წარმატებით შესრულდა']));
        }
        throw new HttpResponseException(response()
            ->json(['StatusCode' => 0, 'StatusMessage' => 'დაფიქსირდა შეცდომა']));
    }

    public function Update(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new Admin();
        $user = $Item::find($ID);
        $user->name = $request->input('Name');
        if ($request->has('Password') && !is_null($request->input('Password'))){
            $user->password = Hash::make($request->input('Password'));
        }

        if ($user->save()){
            $user->removeRole($user->getRoleNames()[0]);
            $user->assignRole($request->input('RoleName'));
            throw new HttpResponseException(response()
                ->json(['StatusCode' => 1, 'StatusMessage' => 'ოპერაცია წარმატებით შესრულდა']));
        }
        throw new HttpResponseException(response()
            ->json(['StatusCode' => 0, 'StatusMessage' => 'დაფიქსირდა შეცდომა']));
    }

    public function getRoles()
    {
        return Role::all();
    }
}
