<?php

namespace App\Repositories;

use App\Helpers\Helpers;
use App\Repositories\Interfaces\LocationsRepositoryIterface;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class LocationsRepository implements LocationsRepositoryIterface
{


    public function Regions(Request $request)
    {
        return Helpers::LoadLocatios($request);
    }

    public function RegionsById(int $Id)
    {
        return DB::table('regions')->find($Id);
    }

    public function Districts(Request $request)
    {
        return Helpers::LoadLocatios($request);
    }

    public function DistrictsById(int $Id)
    {
        return DB::table('districts')->find($Id);
    }

    public function Urban(Request $request)
    {
        return Helpers::LoadLocatios($request);
    }

    public function UrbanById(int $Id)
    {
        return DB::table('urban')->find($Id);
    }

    public function Streets(Request $request)
    {
        return Helpers::LoadLocatios($request);
    }

    public function StreetsById(int $Id)
    {
        return DB::table('streets')->find($Id);
    }

    public function Create(Request $request)
    {
        if ($request->has('SubID') && !is_null($request->input('SubID'))){
            DB::table($request->PostTable)->insert(
                ['title_ka' => $request->{'Title-ka'}, 'title_en' => $request->{'Title-en'}, 'title_ru' => $request->{'Title-ru'}, $request->PostFrom.'_id' => $request->input('SubID')]
            );

        } else{
            DB::table($request->PostTable)->insert(
                ['title_ka' => $request->{'Title-ka'}, 'title_en' => $request->{'Title-en'}, 'title_ru' => $request->{'Title-ru'}]
            );
        }

        throw new HttpResponseException(response()
            ->json(['StatusCode' => 1,
                'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']));
    }

    public function Update(Request $request)
    {

        if ($request->has('SubID') && !is_null($request->input('SubID'))){
            DB::table($request->PostTable)->where('id', $request->ID)->update([$request->PostFrom.'_id' => $request->input('SubID')]);
        }
        foreach (localization()->getSupportedLocalesKeys() as $Lang) {
            DB::table($request->PostTable)->where('id', $request->ID)->update(['title_'.$Lang.'' => $request->{'Title-'.$Lang}]);
        }

        throw new HttpResponseException(response()
            ->json(['StatusCode' => 1,
                'StatusMessage' => 'ოპერაცია წარმატებით დასრულდა!']));
    }
}
