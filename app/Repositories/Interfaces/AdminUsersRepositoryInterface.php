<?php
namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface AdminUsersRepositoryInterface
{
    public function getUsers();

    public function UsersById(int $Id);

    public function getRoles();

    public function Create(Request $request);

    public function Update(Request $request);


}
