<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface FileRepositoryInterface
{

    Public function UploadFiles(Request $request);

}
