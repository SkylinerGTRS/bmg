<?php


namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

interface MetaRepositoryInterface
{

    public function Back();

    public function Create(Request $request);

    public function Update(Request $request);

}
