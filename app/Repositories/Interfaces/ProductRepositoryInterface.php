<?php
namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface ProductRepositoryInterface
{
    public function Front();

    public function Back();

    public function ShowF(int $Id);

    public function ShowWithLimit(int $id, int $limit);

    public function ShowB(int $Id);

    public function Create(Request $request);

    public function Update(Request $request);

    public function GetAttrs(int $CatId);

    public function GetSelectedRegion(int $region_id);

    public function GetSelectedDistrict(int $district_id);

    public function GetSelectedUrban(int $urban_id);

    public function GetSelectedStreet(int $street_id);

    public function ProductsAjaxFilter(Request $request);

}
