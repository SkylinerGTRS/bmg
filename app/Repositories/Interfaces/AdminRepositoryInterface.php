<?php
namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

/**
 * Interface AdminRepositoryInterface
 * @package App\Repositories
 */
interface AdminRepositoryInterface
{
    public function SaveTasks(Request $request);

    public function AddListItem(Request $request);

    public function ViewListItem(int $PostID, string$PostTable);

    public function EditListItem(Request $request);

    public function DeleteListItem(int $PostID, string $PostTable);

    public function UpdateSubListItems(int $ParentID);

    public function ChangeListItemStatus(int $PostID, string$PostTable);

    public function ShowEditModal(int $Id, string $PostTable);

    public function EditModalPost(Request $request);

    public function ChangeStatus(Request $request);

    public function ChangeSliderStatus(Request $request);

    public function ChangeMainStatus(Request $request);

    public function DeletePost(Request $request);

    public function ChangeListItemSortOrder(Request $request);

    public function ChangeTableItemSortOrder(Request $request);
}
