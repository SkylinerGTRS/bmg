<?php
namespace App\Repositories\Interfaces;


/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface BaseRepositoryInterface
{

    public function GetFiles(int $Id, string $Route);

    public function GetMeta(int $Id);
}
