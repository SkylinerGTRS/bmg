<?php
namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface LocationsRepositoryIterface
{
    public function Regions(Request $request);

    public function RegionsById(int $Id);

    public function Districts(Request $request);

    public function DistrictsById(int $Id);

    public function Urban(Request $request);

    public function UrbanById(int $Id);

    public function Streets(Request $request);

    public function StreetsById(int $Id);

    public function Create(Request $request);

    public function Update(Request $request);

}
