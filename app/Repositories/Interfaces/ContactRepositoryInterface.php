<?php
namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface ContactRepositoryInterface
{
    public function Front();

    public function Back();

    public function ShowF(int $Id);

    public function ShowWithLimit(int $id, int $limit);

    public function ShowB(int $Id);

    public function Create(Request $request);

    public function Update(Request $request);


}
