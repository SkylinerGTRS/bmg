<?php

namespace App\Repositories;

use App\Models\Services;
use App\Helpers\Helpers;
use App\Repositories\Interfaces\ServicesRepositoryInterface;
use App\Scopes\StatusScopes\ActiveScope;
use Illuminate\Http\Request;

class ServicesRepository implements ServicesRepositoryInterface
{

    public function Front()
    {
        return Services::with('MainImage')->orderBy('created_at', 'desc')->get();
    }

    public function Back()
    {
        return Services::withoutGlobalScope(ActiveScope::class)->orderBy('created_at', 'asc')->get();
    }

    public function ShowF(int $Id)
    {
        return Services::with('MainImage')->orderBy('created_at', 'desc')->findOrFail($Id);
    }

    public function ShowB(int $Id)
    {
        return Services::withoutGlobalScope(ActiveScope::class)->find($Id);
    }

    public function Create(Request $request)
    {
        $Services = new Services;
        $Services->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Services->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Services,'services');
    }

    public function Update(Request $request)
    {
        $ID = $request->input('ID');
        $Item = new Services;
        $Services = $Item::withoutGlobalScope(ActiveScope::class)->find($ID);
        $Services->setTranslations('title', Helpers::GenerateTrans($request, 'Title-'));
        $Services->setTranslations('descr', Helpers::GenerateTrans($request, 'Descr-'));
        Helpers::Save($request, $Services,'services');
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return Services::with('MainImage')
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $id)->orderByRaw('updated_at DESC')
            ->Limit($limit)
            ->get();
    }
}
