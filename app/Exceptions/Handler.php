<?php

namespace App\Exceptions;

use App\Models\Log;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;

class Handler extends ExceptionHandler
{
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['message' => $exception->getMessage()], 401);
        }
        $guard = Arr::get($exception->guards(), 0);
        switch($guard){
            case 'admin':
                $login = 'admin.login';
                break;
            default:
                $login = 'home';
                break;
        }
        return redirect()->guest(route($login));
    }

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    public function report(\Throwable $exception)
    {
        Log::create([
            'message' => $exception,
            'code' => $exception->getCode(),
            'line' => $exception->getLine(),
        ]);
        parent::report($exception);
    }

    public function render($request, \Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    public function register()
    {
        //
    }
}
