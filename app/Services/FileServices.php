<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\FileRepository;
use App\Repositories\Interfaces\FileRepositoryInterface;
use Illuminate\Http\Request;

class FileServices implements FileRepositoryInterface
{
    private $fileRepository;

    public function __construct(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    Public function UploadFiles(Request $request)
    {
        return $this->fileRepository->UploadFiles($request);
    }
}
