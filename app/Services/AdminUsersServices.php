<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\AdminUsersRepository;
use App\Repositories\Interfaces\AdminUsersRepositoryInterface;
use Illuminate\Http\Request;

class AdminUsersServices implements AdminUsersRepositoryInterface
{
    private $adminUsersRepository;

    public function __construct(AdminUsersRepository $adminUsersRepository)
    {
        $this->adminUsersRepository = $adminUsersRepository;
    }

    public function getUsers()
    {
        return $this->adminUsersRepository->getUsers();

    }

    public function UsersById(int $Id)
    {
        return $this->adminUsersRepository->UsersById($Id);
    }

    public function Create(Request $request)
    {
        return $this->adminUsersRepository->Create($request);
    }

    public function GetRoles()
    {
        return $this->adminUsersRepository->GetRoles();
    }

    public function Update(Request $request)
    {
        return $this->adminUsersRepository->Update($request);
    }
}
