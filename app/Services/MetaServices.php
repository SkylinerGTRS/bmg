<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\Interfaces\MetaRepositoryInterface;
use App\Repositories\MetaRepository;
use Illuminate\Http\Request;

class MetaServices implements MetaRepositoryInterface
{
    private $metaRepository;

    public function __construct(MetaRepository $metaRepository)
    {
        $this->metaRepository = $metaRepository;
    }

    public function Back()
    {
        return $this->metaRepository->Back();
    }

    public function Create(Request $request)
    {
        return $this->metaRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->metaRepository->Update($request);
    }

    public function Show(int $Id)
    {
        return $this->metaRepository->Show($Id);
    }
}
