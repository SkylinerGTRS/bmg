<?php
namespace App\Services;


use App\Repositories\AdminRepository;
use App\Repositories\Interfaces\AdminRepositoryInterface;
use Illuminate\Http\Request;

class AdminServices implements AdminRepositoryInterface
{
    private $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function SaveTasks(Request $request)
    {
        return $this->adminRepository->SaveTasks($request);
    }

    public function AddListItem(Request $request)
    {
        return $this->adminRepository->AddListItem($request);
    }

    public function ViewListItem(int $PostID, string $PostTable)
    {
        return $this->adminRepository->ViewListItem($PostID, $PostTable);
    }

    public function EditListItem(Request $request)
    {
        return $this->adminRepository->EditListItem($request);
    }

    public function DeleteListItem(int $PostID, string $PostTable)
    {
        return $this->adminRepository->DeleteListItem($PostID, $PostTable);
    }

    public function UpdateSubListItems(int $ParentID)
    {
        return $this->adminRepository->UpdateSubListItems($ParentID);
    }

    public function ChangeListItemStatus(int $PostID, string $PostTable)
    {
        return $this->adminRepository->ChangeListItemStatus($PostID, $PostTable);
    }

    public function ShowEditModal(int $Id, string $PostTable)
    {
        return $this->adminRepository->ShowEditModal($Id, $PostTable);
    }

    public function EditModalPost(Request $request)
    {
        return $this->adminRepository->EditModalPost($request);
    }

    public function ChangeListMainStatus(Request $request)
    {
        return $this->adminRepository->ChangeListMainStatus($request);

    }

    public function ChangeStatus(Request $request)
    {
        return $this->adminRepository->ChangeStatus($request);
    }

    public function ChangeSliderStatus(Request $request)
    {
        return $this->adminRepository->ChangeSliderStatus($request);
    }

    public function ChangeMainStatus(Request $request)
    {
        return $this->adminRepository->ChangeMainStatus($request);
    }

    public function DeletePost(Request $request)
    {
        return $this->adminRepository->DeletePost($request);
    }

    public function ChangeListItemSortOrder(Request $request)
    {
        return $this->adminRepository->ChangeListItemSortOrder($request);
    }

    public function ChangeTableItemSortOrder(Request $request)
    {
        return $this->adminRepository->ChangeTableItemSortOrder($request);
    }
}
