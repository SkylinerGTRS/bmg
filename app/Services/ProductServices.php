<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductServices implements ProductRepositoryInterface
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function Front()
    {
        return $this->productRepository->Front();
    }

    public function Back()
    {
        return $this->productRepository->Back();
    }

    public function ShowF(int $Id)
    {
        return $this->productRepository->ShowF($Id);
    }
    public function GetAttrs(int $CatId) {

        return $this->productRepository->GetAttrs($CatId);
    }

    public function GetSelectedAttrs(int $Id) {

        return $this->productRepository->GetSelectedAttrs($Id);
    }

    public function ShowB(int $Id)
    {
        return $this->productRepository->ShowB($Id);
    }

    public function Create(Request $request)
    {
        return $this->productRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->productRepository->Update($request);
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return $this->productRepository->ShowWithLimit($id,$limit);
    }

    public function GetSelectedRegion(int $region_id)
    {
        return $this->productRepository->GetSelectedRegion($region_id);
    }

    public function GetSelectedDistrict(int $district_id)
    {
        return $this->productRepository->GetSelectedDistrict($district_id);
    }

    public function GetSelectedUrban(int $urban_id)
    {
        return $this->productRepository->GetSelectedUrban($urban_id);
    }

    public function GetSelectedStreet(int $street_id)
    {
        return $this->productRepository->GetSelectedStreet($street_id);
    }

    public function ProductsAjaxFilter(Request $request)
    {
        return $this->productRepository->ProductsAjaxFilter($request);
    }

    public function MainCats(int $id)
    {
        return $this->productRepository->MainCats($id);
    }

    public function ProductCats(int $id)
    {
        return $this->productRepository->ProductCats($id);
    }

    public function ShowSameProducts(int $id, int $type_id, int $urban_id, int $deal_id, int $price_type, int $district_id, int $limit)
    {
        return $this->productRepository->ShowSameProducts($id, $type_id, $urban_id, $deal_id, $price_type, $district_id, $limit);
    }

    public function IncreaseViews($Id)
    {
        return $this->productRepository->IncreaseViews($Id);
    }

    public function GetRegions()
    {
        return $this->productRepository->GetRegions();
    }

    public function MinPrice()
    {
        return $this->productRepository->MinPrice();
    }

    public function MaxPrice()
    {
        return $this->productRepository->MaxPrice();
    }

    public function SearchResults(Request $request)
    {
        return $this->productRepository->SearchResults($request);
    }
}
