<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\ContactRepository;
use App\Repositories\Interfaces\ContactRepositoryInterface;
use Illuminate\Http\Request;

class ContactServices implements ContactRepositoryInterface
{
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function Front()
    {
        return $this->contactRepository->Front();
    }

    public function Back()
    {
        return $this->contactRepository->Back();
    }

    public function ShowF(int $Id)
    {
        return $this->contactRepository->ShowF($Id);
    }

    public function ShowB(int $Id)
    {
        return $this->contactRepository->ShowB($Id);
    }

    public function Create(Request $request)
    {
        return $this->contactRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->contactRepository->Update($request);
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return $this->contactRepository->ShowWithLimit($id,$limit);
    }
}
