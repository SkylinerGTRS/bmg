<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use App\Repositories\Interfaces\OrdersRepositoryInterface;
use Illuminate\Http\Request;

class OrdersServices implements OrdersRepositoryInterface
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function AddStatementPost(Request $request)
    {
        return $this->orderRepository->AddStatementPost($request);
    }

    public function NotCheckedOrders()
    {
        return $this->orderRepository->NotCheckedOrders();
    }

    public function OrdersById(int $ID)
    {
        return $this->orderRepository->OrdersById($ID);
    }

    public function EditOrdersPost(Request $request)
    {
        return $this->orderRepository->EditOrdersPost($request);
    }

    public function CheckedOrders()
    {
        return $this->orderRepository->CheckedOrders();
    }
}
