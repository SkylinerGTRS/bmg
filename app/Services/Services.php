<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;


use App\Repositories\Interfaces\ServicesRepositoryInterface;
use App\Repositories\ServicesRepository;
use Illuminate\Http\Request;

class Services implements ServicesRepositoryInterface
{
    private $servicesRepository;

    public function __construct(ServicesRepository $servicesRepository)
    {
        $this->servicesRepository = $servicesRepository;
    }

    public function Front()
    {
        return $this->servicesRepository->Front();
    }

    public function Back()
    {
        return $this->servicesRepository->Back();
    }

    public function ShowF(int $Id)
    {
        return $this->servicesRepository->ShowF($Id);
    }

    public function ShowB(int $Id)
    {
        return $this->servicesRepository->ShowB($Id);
    }

    public function Create(Request $request)
    {
        return $this->servicesRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->servicesRepository->Update($request);
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return $this->servicesRepository->ShowWithLimit($id,$limit);
    }
}
