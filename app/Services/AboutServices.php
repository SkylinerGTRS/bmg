<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\AboutRepository;
use App\Repositories\Interfaces\AboutRepositoryInterface;
use Illuminate\Http\Request;

class AboutServices implements AboutRepositoryInterface
{
    private $aboutRepository;

    public function __construct(AboutRepository $aboutRepository)
    {
        $this->aboutRepository = $aboutRepository;
    }

    public function Front(int $ID = null)
    {
        return $this->aboutRepository->Front($ID);
    }

    public function AboutMain()
    {
        return $this->aboutRepository->AboutMain();
    }

    public function Back()
    {
        return $this->aboutRepository->Back();
    }

    public function ShowF(int $Id)
    {
        return $this->aboutRepository->ShowF($Id);
    }

    public function ShowB(int $Id)
    {
        return $this->aboutRepository->ShowB($Id);
    }

    public function Create(Request $request)
    {
        return $this->aboutRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->aboutRepository->Update($request);
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return $this->aboutRepository->ShowWithLimit($id,$limit);
    }
}
