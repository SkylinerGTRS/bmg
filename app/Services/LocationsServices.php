<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\Interfaces\LocationsRepositoryIterface;
use App\Repositories\LocationsRepository;
use Illuminate\Http\Request;

class LocationsServices implements LocationsRepositoryIterface
{
    private $locationsRepository;

    public function __construct(LocationsRepository $locationsRepository)
    {
        $this->locationsRepository = $locationsRepository;
    }

    public function Regions(Request $request)
    {
        return $this->locationsRepository->Regions($request);
    }

    public function RegionsById(int $Id)
    {
        return $this->locationsRepository->RegionsById($Id);
    }

    public function Districts(Request $request)
    {
        return $this->locationsRepository->Districts($request);
    }

    public function DistrictsById(int $Id)
    {
        return $this->locationsRepository->DistrictsById($Id);
    }

    public function Urban(Request $request)
    {
        return $this->locationsRepository->Urban($request);
    }

    public function UrbanById(int $Id)
    {
        return $this->locationsRepository->UrbanById($Id);
    }

    public function Streets(Request $request)
    {
        return $this->locationsRepository->Streets($request);
    }

    public function StreetsById(int $Id)
    {
        return $this->locationsRepository->StreetsById($Id);
    }

    public function Create(Request $request)
    {
        return $this->locationsRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->locationsRepository->Update($request);
    }
}
