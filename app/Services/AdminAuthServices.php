<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AdminAuthServices
{
    public function username()
    {
        return 'username';
    }
    private function redirectTo()
    {
        return  '/admin';
    }
    public function login($request){
        $data = [
            $this->username() => $request->input($this->username()),
            'password' => $request->input('password')
        ];
        if (Auth::guard('admin')->attempt($data)) {
            return redirect()->to($this->redirectTo());
        } else {
            throw ValidationException::withMessages([$this->username() => [trans('auth.failed')]]);
        }
    }

    public function logout($request){
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        return redirect()->route('admin.login');
    }
}
