<?php /** @noinspection PhpVoidFunctionResultUsedInspection */

namespace App\Services;

use App\Repositories\BlogRepository;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use Illuminate\Http\Request;

class BlogServices implements BlogRepositoryInterface
{
    private $blogRepository;

    public function __construct(BlogRepository $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }

    public function Front(int $ID = null, int $paginate = null )
    {
        return $this->blogRepository->Front($ID, $paginate);
    }

    public function Back()
    {
        return $this->blogRepository->Back();
    }

    public function ShowF(int $Id)
    {
        return $this->blogRepository->ShowF($Id);
    }

    public function ShowB(int $Id)
    {
        return $this->blogRepository->ShowB($Id);
    }

    public function Create(Request $request)
    {
        return $this->blogRepository->Create($request);
    }

    public function Update(Request $request)
    {
        return $this->blogRepository->Update($request);
    }

    public function ShowWithLimit(int $id, int $limit)
    {
        return $this->blogRepository->ShowWithLimit($id,$limit);
    }

    public function MainBlog()
    {
        return $this->blogRepository->MainBlog();
    }
}
