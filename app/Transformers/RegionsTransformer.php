<?php
namespace App\Transformers;

use League\Fractal;

class RegionsTransformer extends Fractal\TransformerAbstract
{

    protected $posttable;

    public function __construct($posttable) {
        $this->posttable = $posttable;
    }
    public function transform($locations)
    {

        return [
            "id" => (int) $locations->id,
            "title_ka" => (string) $locations->title_ka,
            "title_en" => (string) $locations->title_en,
            "title_ru" => (string) $locations->title_ru,
            "actions" => '<td style="float: right;">
                        <a href="#"><span class="circle circle-sm bg-danger di" onclick="return admin.deletePost(this,`'.$this->posttable.'`,`'.$locations->id.'`);"><i class="ti-trash"></i></span><span></span></a>
                        <a href="'.route('admin.locations.'.$this->posttable.'.edit',[$locations->id]).'"><span class="circle circle-sm bg-success di"><i class="ti-pencil-alt"></i></span><span></span></a>
                        </td>'
        ];
    }
}
