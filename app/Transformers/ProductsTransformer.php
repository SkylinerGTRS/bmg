<?php
namespace App\Transformers;

use App\Models\Products;
use League\Fractal;

class ProductsTransformer extends Fractal\TransformerAbstract
{
    public function transform(Products $products)
    {
        if ($products->status_id == 1){
            $status = 'green-status';
        } else {
            $status = 'red-status';
        }
        if (auth()->user()->can('edit products')){
            $edittext = '<a href="'.route('admin.products.edit',[$products->id]).'"><span class="circle circle-sm bg-success di"><i class="ti-pencil-alt"></i></span><span></span></a>';

        } else {
            $edittext = '';
        }
        if (auth()->user()->can('delete products')){
            $deletetext = '<a href="#"><span class="circle circle-sm bg-danger di" onclick="return admin.deletePost(this,`products`,`'.$products->id.'`);"><i class="ti-trash"></i></span><span></span></a>';
        } else {
            $deletetext = '';
        }

        if (auth()->user()->can('publish products')){
            $statustext = '<span class="circle text-status '.$status.'" data-toggle="tooltip" data-placement="top" title="" onclick="admin.changePostStatus(this, `products`,`'.$products->id.'`)" data-original-title="Status">
            			<i class="ti-eye"></i></span>';
        } else {
            $statustext = '';
        }
        return [
            "id" => (int) $products->id,
            "title" => (string) $products->title,
            "deal_id" => (string) $products->Deal->title,
            "type_id" => (string) $products->Property->title,
            "price_gel" => (int) $products->price_gel.'₾',
            "created_at" => ($products->created_at)->format('Y/m/d - H:m'),
            "actions" => '<td style="float: right;">'
                            .$statustext.
                            $deletetext.
                            $edittext.
                         '</td>'
        ];
    }
}
