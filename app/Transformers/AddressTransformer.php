<?php
namespace App\Transformers;

use App\Models\Address;
use League\Fractal;

class AddressTransformer extends Fractal\TransformerAbstract
{
    public function transform($address)
    {
        return [
            'id'      => (int) $address->id,
            'text'    => $address->title_ka,
        ];
    }
}
