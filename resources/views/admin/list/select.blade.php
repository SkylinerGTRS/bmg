{% for val in list %}
	<option value="{{ val.id }}" {{ (val.children|length and disabled ? 'disabled') ~ (val.id == id ? 'selected') }}>{{ (tabs ~ val.title)|raw }}</option>
	{% if val.children|length %}
		{% include "admin/list/select.tpl" with {list: val.children, id: id, disabled: disabled, tabs: tabs ~ '&nbsp;&nbsp;&nbsp;'} only %}
	{% endif %}
{% endfor %}