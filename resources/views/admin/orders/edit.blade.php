@extends('admin_layouts.dashboard')
@section('header')
    <script>
        let Json = '{!! $Files !!}';
        let Data = JSON.parse(Json);
    </script>
    <style>
        .dropzone .dz-preview .dz-remove {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.orders')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active"><a href="{{route('admin.orders.notchecked')}}">{{ Lang::get('global.notchecked')}}</a></li>
                        <li class="active">{{$item->name}} {{$item->lastname}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <form data-toggle="validator" action="{{ route('admin.orders.editpost') }}"
                                  data-success-url="{{ route('admin.orders.notchecked') }}" method="POST"
                                  onsubmit="return admin.savePost(this)">
                                @CSRF
                                <div class="col-sm-12">
                                        <div class="form-group col-sm-6">
                                            <label for="exampleInputuname">{{Lang::get('global.name')}}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input type="text" class="form-control" id="exampleInputuname" value="{{$item->name}}" disabled>
                                            </div>
                                        </div>
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputuname">{{Lang::get('global.lastname')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="exampleInputuname" value="{{$item->lastname}}" disabled>
                                        </div>
                                    </div>
                                        <div class="form-group col-sm-6">
                                            <label for="exampleInputEmail1">{{Lang::get('global.email')}}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                                <input type="email" class="form-control" id="exampleInputEmail1" value="{{$item->email}}" disabled> </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="exampleInputphone">{{Lang::get('global.phone')}}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input type="tel" class="form-control" id="exampleInputphone" value="{{$item->phone}}" disabled> </div>
                                        </div>
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputphone">{{Lang::get('global.code')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-shortcode"></i></div>
                                            <input type="tel" class="form-control" id="exampleInputphone" value="{{$item->code}}" disabled> </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputphone">{{Lang::get('global.price')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-printer"></i></div>
                                            <input type="tel" class="form-control" id="exampleInputphone" value="{{$item->price}} {{$item->currency == 'gel' ? '₾' : '$'}}" disabled> </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="message">{{Lang::get('global.descr')}}</label>
                                        <textarea type="text" class="form-control" id="message" disabled style="height: 300px">{{$item->message}}</textarea>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <p style="font-weight: bold">{{Lang::get('global.pictures')}}</p>
                                        <div class="dropzone" id="my-awesome-dropzone"></div>
                                        <div class="btnWrapper">
                                            @if ($item->checked == 0)
                                                <input type="hidden" name="ID" value="{{$item->id}}">
                                                <button type="submit" class="btn btn-success">{{ Lang::get('global.mark_as_checked')}}</button>
                                                <a href="{{route('admin.orders.notchecked')}}"> <button type="button" class="btn btn-inverse">{{ Lang::get('global.go_back')}}</button></a>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2020 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script type="text/javascript" data-accepted_files=".jpeg,.jpg,.png" data-PostTable="orders" data-max_files="15" src="/admin_assets/js/file.js"></script>
    <script>
        jQuery(document).ready(function() {
            $("div#my-awesome-dropzone").dropzone({
                clickable: false
            });
        });
    </script>
@endsection


