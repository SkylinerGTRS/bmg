@extends('admin_layouts.dashboard')
@section('header')
    <link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.users')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="{{route('admin.users')}}">{{ Lang::get('global.users')}}</a></li>
                        <li class="active">{{$item->name}} - {{ Lang::get('global.edit')}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <form data-toggle="validator" action="{{ route('admin.users.editpost') }}"
                                  data-success-url="{{ route('admin.users') }}" method="POST"
                                  onsubmit="return admin.savePost(this)">
                                @CSRF
                                <div class="form-group col-sm-6">
                                    <label for="Name" class="control-label">{{Lang::get('global.name')}}</label>
                                    <input type="text" id="Name" class="form-control" placeholder="Name" value="{{$item->name}}" name="Name" required>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="Email" class="control-label">{{Lang::get('global.email')}}</label>
                                    <input type="text" id="Email" class="form-control" value="{{$item->username}}" placeholder="Email" name="username" disabled>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="Password" class="control-label">{{Lang::get('global.new_password')}}</label>
                                    <input type="password" id="Password" class="form-control" placeholder="Password" name="Password">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="Password_confirmation" class="control-label">{{Lang::get('global.repeat_password')}}</label>
                                    <input type="password" id="Password_confirmation" class="form-control" placeholder="Repeat Password" name="Password_confirmation">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="Roles" class="control-label">{{ Lang::get('global.roles')}}</label>
                                    <select id="Roles" name="RoleName" class="form-control select2" style="width:100%;" required>
                                        <option value="">{{Lang::get('global.choose')}}</option>
                                        @foreach($Roles as $role)
                                            <option value="{{$role->name}}" {{$item->getRoleNames()[0] == $role->name ? 'selected' : ''}}>{{$role->slug}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="btnWrapper">
                                        <input type="hidden" name="ID" value="{{$item->id}}">
                                        <button type="submit" class="btn btn-primary">{{ Lang::get('global.submit')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2019 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script src="/admin_assets/js/validator.js"></script>
    <script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
        });
    </script>
@endsection


