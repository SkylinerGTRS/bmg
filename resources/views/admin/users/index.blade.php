@extends('admin_layouts.dashboard')
@section('header')
    <link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.users')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">{{ Lang::get('global.users')}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div style="padding: 20px">
                            <a href="{{ route('admin.users.add') }}" ><button type="button" class="btn btn-info btn-rounded">{{ Lang::get('global.add')}}</button></a>
                        </div>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email/UserName</th>
                                    <th>Role</th>
                                    <th style="width: 250px !important;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Users as $item)
                                    <tr>
                                        <td>{{ $item->id}}</td>
                                        <td>{{ $item->name}}</td>
                                        <td>{{ $item->username}}</td>
                                        <td>{{ $item->getRoleName()[0]->slug}}</td>
                                        <td style="float: right;">
                                            <a href="#"><span class="circle circle-sm bg-danger di" onclick="return admin.deletePost(this,'admins','{{$item->id}}');"><i class="ti-trash"></i></span><span></span></a>
                                            <a href="{{ route('admin.users.edit',[$item->id]) }}"><span class="circle circle-sm bg-success di"><i class="ti-pencil-alt"></i></span><span></span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2019 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable({
                "order": [[0, 'desc']]
            });
        });
    </script>
@endsection


