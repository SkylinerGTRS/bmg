@extends('admin_layouts.dashboard')
@section('header')
    <link href="/plugins/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script>
        let Data = '';
    </script>
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.statements')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li>Admin</li>
                        <li><a href="{{route('admin.products')}}">{{ Lang::get('global.statements')}}</a></li>
                        <li class="active">{{ Lang::get('global.add')}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <form data-toggle="validator" action="{{ route('admin.products.addpost') }}" data-success-url="{{ route('admin.products') }}" method="POST" onsubmit="return admin.savePost(this)">
                                @CSRF
                                <div class="sttabs tabs-style-linemove">
                                    <nav>
                                        <ul>
                                            <li><a href="#descr" class="sticon ti-align-left"><span>{{ Lang::get('global.descr')}}</span></a></li>
                                            <li><a href="#address" class="sticon ti-control-shuffle"><span>{{ Lang::get('global.address')}}</span></a></li>
                                            <li><a href="#detailed_info" class="sticon ti-agenda"><span>{{ Lang::get('global.detailed_info')}}</span></a></li>
                                            <li><a href="#aditional_details" class="sticon ti-help-alt"><span>{{ Lang::get('global.aditional_details')}}</span></a></li>
                                            <li><a href="#pictures" class="sticon ti-upload"><span>{{ Lang::get('global.pictures')}}</span></a></li>
                                        </ul>
                                    </nav>
                                        <!-- Tab panes -->
                                        <div class="content-wrap">
                                            <section id="descr">
                                                @foreach ($Langs as $Lang)
                                                    <div class="form-group col-sm-4">
                                                        <label for="Title-{{$Lang}}" class="control-label">Title-{{$Lang}}</label>
                                                        <input type="text" class="form-control" id="Title-{{$Lang}}" name="Title-{{$Lang}}" placeholder="{{ Lang::get('global.title')}}" required>
                                                    </div>
                                                @endforeach
                                                    @foreach ($Langs as $Lang)
                                                        <div class="form-group col-sm-4">
                                                            <label for="description" class="control-label">{{ Lang::get('global.descr')}} - {{$Lang}}</label>
                                                            <textarea class="summernote" id="description" name="Descr-{{$Lang}}"> {{ Lang::get('global.descr')}} - {{$Lang}}</textarea>
                                                        </div>
                                                    @endforeach
                                            </section>
                                            <section id="address">
                                                <div class="col-12">
                                                    <div class="form-group col-sm-3">
                                                        <label for="regions" class="control-label">{{Lang::get('global.regions')}}</label>
                                                        <select id="regions" class="form-control locations" name="regionId" style="width:100%;" required>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="districts" class="control-label">{{Lang::get('global.municipalities')}}</label>
                                                        <select id="districts" class="form-control locations" name="districtId" style="width:100%;" required>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="urban" class="control-label">{{Lang::get('global.city_village')}}</label>
                                                        <select id="urban" class="form-control locations" name="urbanId" style="width:100%;" required>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="streets" class="control-label">{{Lang::get('global.streets')}}</label>
                                                        <select id="streets" class="form-control locations" name="streetId" style="width:100%;" required>
                                                        </select>
                                                    </div>

                                                @foreach ($Langs as $Lang)
                                                    <div class="form-group col-md-4">
                                                        <label for="Address-{{$Lang}}" class="control-label">Address-{{$Lang}}</label>
                                                        <input type="text" class="form-control" id="Address-{{$Lang}}" name="Address-{{$Lang}}" placeholder="{{ Lang::get('global.address')}}" required>
                                                    </div>
                                                @endforeach
                                                </div>
                                            </section>
                                            <section id="detailed_info">
                                                <div class="col-12">
                                                    <div class="form-group col-sm-6">
                                                        <label for="Deal" class="control-label">{{ Lang::get('global.deal_types')}}</label>
                                                        <select id="Deal" name="DealId" class="form-control select2" style="width:100%;" required>
                                                            <option value="">{{Lang::get('global.choose')}}</option>
                                                            @foreach($Deal as $item)
                                                                <option value="{{$item->cat_id}}">{{$item->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="price_type" class="control-label">{{ Lang::get('global.price_type')}}</label>
                                                        <select id="price_type" name="price_type" class="form-control select2" style="width:100%;" required>
                                                            <option value="">{{Lang::get('global.choose')}}</option>
                                                            <option value="1">{{Lang::get('global.total_price')}}</option>
                                                            <option value="2">{{Lang::get('global.square_price')}}</option>
                                                            <option value="3">{{Lang::get('global.price_by_agreement')}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="space" class="control-label">{{ Lang::get('global.space')}}</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">&#13217</div>
                                                            <input class="form-control" id="space" type="number" value="" name="space" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="price_usd" class="control-label">{{ Lang::get('global.price_usd')}}</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">$</div>
                                                            <input class="form-control" id="price_usd" type="number" value="" name="price_usd" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="price_gel" class="control-label">{{ Lang::get('global.price_gel')}}</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">₾</div>
                                                            <input class="form-control" id="price_gel" type="number" value="" name="price_gel" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <div class="form-group col-sm-4">
                                                        <div class="checkbox checkbox-primary" id="business">
                                                            <input id="business-1" type="checkbox" name="business" value="1">
                                                            <label for="business-1">{{Lang::get('global.business')}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <div class="checkbox checkbox-primary" id="Vip">
                                                            <input id="Vip-1" type="checkbox" name="Vip" value="1">
                                                            <label for="Vip-1">{{Lang::get('global.vip')}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <div class="checkbox checkbox-primary" id="Exc">
                                                            <input id="Exc-1" type="checkbox" name="Exc" value="1">
                                                            <label for="Exc-1">{{Lang::get('global.exclusive')}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section id="aditional_details">
                                                <div class="col-12">
                                                    @foreach($Attrs as $att)
                                                        @if ($att->options == 1)
                                                            <div class="form-group col-sm-3">
                                                                <label for="Attr-{{$att->cat_id}}" class="control-label">{{$att->title}}</label>
                                                                <input type="text" class="form-control" id="Attr-{{$att->cat_id}}" name="Attr[{{$att->cat_id}}]" placeholder="{{$att->title}}">
                                                            </div>
                                                        @endif
                                                        @if($att->options == 2)
                                                            <div class="form-group col-sm-3">
                                                                <label for="{{$att->cat_id}}" class="control-label">{{$att->title}}</label>
                                                                <select id="{{$att->cat_id}}" name="Attr[{{$att->cat_id}}]" class="form-control select2" style="width:100%;">
                                                                    <option value="">{{Lang::get('global.choose')}}</option>
                                                                    @foreach($att->children as $subatt)
                                                                        <option value="{{$subatt->cat_id}}">{{$subatt->title}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        @elseif ($att->options == 3)
                                                            <div class="form-group col-sm-12">
                                                                <label for="{{$att->cat_id}}" class="control-label">{{$att->title}}</label>
                                                                @foreach($att->children as $subatt)
                                                                    <div class="checkbox checkbox-primary" id="{{$att->cat_id}}">
                                                                        <input id="att-{{$subatt->cat_id}}" type="checkbox" name="Attr[{{$subatt->cat_id}}]" value="{{$subatt->cat_id}}">
                                                                        <label for="att-{{$subatt->cat_id}}"> {{$subatt->title}} </label>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>

                                                <div class="clearfix"></div>
                                            </section>
                                            <section id="pictures">
                                                    <div class="form-group">
                                                        <p>სურათის ზომა - 1200/800</p>
                                                        <div class="dropzone" id="my-awesome-dropzone"></div>
                                                    </div>
                                            </section>
                                        </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="btnWrapper">
                                            <input type="hidden" name="TypeId" value="{{$Type_id}}">
                                            <button type="submit" class="btn btn-primary">{{ Lang::get('global.submit')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2020 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script src="/admin_assets/js/validator.js"></script>
    <script src="/plugins/bower_components/summernote/dist/summernote.min.js"></script>
    <script type="text/javascript" data-accepted_files=".jpeg,.jpg,.png,.gif" data-PostTable="products" data-max_files="10" src="/admin_assets/js/file.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="/admin_assets/js/cbpFWTabs.js"></script>
    <script type="text/javascript">
        (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();
    </script>
    <script>
        jQuery(document).ready(function() {
            $('.select2').select2();

            $('.locations').each(function() {
                let thisId;
                let parent;
                var type = this.id;
                $(this).select2({
                    ajax: {
                        url: '{{route('admin.loadlocations')}}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            if (type === 'regions'){
                                thisId = null;
                            } else if(type === 'districts'){
                                thisId = $('#regions :selected').val();
                                parent = 'regions';
                            } else if(type === 'urban'){
                                thisId = $('#districts :selected').val();
                                parent = 'districts';
                            }else if(type === 'streets') {
                                thisId = $('#urban :selected').val();
                                parent = 'urban';
                            }
                            return {
                                search: params.term, // search term
                                page: params.page,
                                type: type,
                                parent: parent,
                                id : thisId
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            return {
                                results: data.items.data,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },

                    placeholder: '{{Lang::get('global.search_for')}}',
                    minimumInputLength: 2,
                });

            });

            $('.summernote').summernote({
                height: 300, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
        });
    </script>
@endsection


