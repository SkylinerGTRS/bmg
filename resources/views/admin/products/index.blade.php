@extends('admin_layouts.dashboard')
@section('header')
    <link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.statements')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">{{ Lang::get('global.statements')}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div style="padding: 20px">
                            @can('create products')
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><button type="button" class="btn btn-info btn-rounded">{{ Lang::get('global.add')}}</button></a>
                            @endcan
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <button class="btn btn-warning waves-effect waves-light collapseble" style="float: right;margin-top: -40px"><span>{{ Lang::get('global.filter')}}</span> <i class="fa fa-filter m-l-5"></i></button>
                                <div class="m-t-15 collapseblebox dn">
                                    <div class="well">
                                        <div class="col-12">
                                            <div class="form-group col-sm-6">
                                                <label for="DealType" class="control-label">{{ Lang::get('global.deal_types')}}</label>
                                                <select id="DealType" name="DealId" class="form-control select2" style="width:100%;" required>
                                                    <option value="">{{Lang::get('global.choose')}}</option>
                                                    @foreach($Deal as $item)
                                                        <option value="{{$item->cat_id}}">{{$item->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="PropertyType" class="control-label">{{ Lang::get('global.property_type')}}</label>
                                                <select id="PropertyType" name="price_type" class="form-control select2" style="width:100%;">
                                                    <option value="">{{Lang::get('global.choose')}}</option>
                                                    @foreach($Property as $item)
                                                        <option value="{{$item->cat_id}}">{{$item->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label for="price_from" class="control-label">{{ Lang::get('global.price')}} - {{ Lang::get('global.from')}}</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">₾</div>
                                                    <input class="form-control" id="price_from" type="number">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label for="price_to" class="control-label">{{ Lang::get('global.price')}} - {{ Lang::get('global.to')}}</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">₾</div>
                                                    <input class="form-control" id="price_to" type="number">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="search-id" class="control-label">{{ Lang::get('global.search_by_id')}}</label>
                                                <input class="form-control" id="search-id" type="number">
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="search-title" class="control-label">{{ Lang::get('global.search_by_title')}}</label>
                                                <input class="form-control" id="search-title" type="text">
                                            </div>
                                        </div>
                                        <div class="row mt-8">
                                            <div class="col-lg-12">
                                                <button class="btn btn-primary btn-primary--icon" id="dt_search" style="float: right">
													<span>
														<i class="fa fa-search"></i>
														<span>Search</span>
													</span>
                                                </button>&nbsp;&nbsp;
                                                <button class="btn btn-secondary btn-secondary--icon" id="dt_reset" style="float: right; margin-right: 20px">
													<span>
														<i class="fa fa-close"></i>
														<span>Reset</span>
													</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Deal Type</th>
                                    <th>Property Type</th>
                                    <th>Price</th>
                                    <th>Created At</th>
                                    <th style="width: 250px !important;">Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{Lang::get('global.choose_type')}}</h4> </div>
                    <div class="modal-body">
                        @foreach ($MainCats as $item)
                            <div class="form-group">
                                <a href="{{ route('admin.products.add',[$item->cat_id]) }}"><button type="button" class="btn btn-info btn-rounded">{{$item->title}}</button></a>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">{{Lang::get('global.close')}}</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2019 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            var table = $('#myTable').DataTable({
                processing : true,
                serverSide: true,
                searching: false,
                ordering: false,
                ajax: {
                    url: '{{route('admin.productsajax')}}',
                    type: 'POST',
                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr("content")
                    },
                    'data': function(data){
                        // Read values
                        var deal = $('#DealType').val();
                        var property = $('#PropertyType').val();
                        var price_from = $('#price_from').val();
                        var price_to = $('#price_to').val();
                        var id = $('#search-id').val();
                        var title = $('#search-title').val();

                        // Append to data
                        data.searchByDeal= deal;
                        data.searchByProperty = property;
                        data.price_from = price_from;
                        data.price_to = price_to;
                        data.id = id;
                        data.title = title;
                    },

                },
                columns: [
                    { data: 'id' },
                    { data: 'title' },
                    { data: 'deal_id' },
                    { data: 'type_id' },
                    { data: 'price_gel' },
                    { data: 'created_at' },
                    { data: 'actions' },
                ]
            });
            $('#dt_search').click( function(e) {
                e.preventDefault();
                table.draw();
            });

            $('#dt_reset').click( function(e) {
                //e.preventDefault();
                location.reload();
            });
        });
    </script>
@endsection


