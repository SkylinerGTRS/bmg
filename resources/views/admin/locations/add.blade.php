@extends('admin_layouts.dashboard')
@section('header')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.'.$PostTo.'')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="{{ route('admin.locations.'.$PostTo.'') }}">{{ Lang::get('global.'.$PostTo.'')}}</a></li>
                        <li class="active">{{ Lang::get('global.edit')}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <form data-toggle="validator" action="{{ route('admin.locations.addpost') }}"
                                  data-success-url="{{ route('admin.locations.'.$PostTo.'') }}" method="POST"
                                  onsubmit="return admin.savePost(this)">
                                @CSRF
                                <div class="col-md-12">
                                    @if (isset($PostFrom) && $PostTo != 'streets')
                                        <div class="form-group">
                                            <label for="SubID" class="control-label">{{ Lang::get('global.'.$PostFrom.'')}}</label>
                                            <select id="SubID" name="SubID" class="form-control select2" style="width:100%;" required>
                                                <option value="">{{Lang::get('global.choose')}}</option>
                                                @foreach($SubLoc as $loc)
                                                    <option value="{{$loc->id}}">{{$loc->title_ka}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @elseif($PostTo != 'regions')
                                        <div class="form-group">
                                            <label for="Urban" class="control-label">{{ Lang::get('global.'.$PostFrom.'')}}</label>
                                            <select id="Urban" name="SubID" class="form-control select2" style="width:100%;" required>
                                                <option value="">{{Lang::get('global.choose')}}</option>
                                            </select>
                                        </div>
                                    @endif
                                    @foreach ($Langs as $Lang)
                                        <div class="form-group">
                                            <label for="Title-{{$Lang}}" class="control-label">Title-{{$Lang}}</label>
                                            <input type="text" class="form-control" id="Title-{{$Lang}}"
                                                   name="Title-{{$Lang}}" value=""
                                                   placeholder="{{ Lang::get('global.title')}}" required>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group">
                                    <div class="btnWrapper">
                                        <input type="hidden" name="PostTable" value="{{$PostTo}}">
                                        <input type="hidden" name="PostFrom" value="{{isset($PostFrom) ? $PostFrom : ''}}">
                                        <button type="submit" class="btn btn-primary">{{ Lang::get('global.add')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2019 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script src="/admin_assets/js/validator.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            $('#Urban').select2(
                {
                    ajax: {
                        url: '{{route('admin.loadlocations')}}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term, // search term
                                page: params.page,
                                type: 'streets',
                                parent: 'urban',
                                id : ''
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            return {
                                results: data.items.data,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },

                    placeholder: '{{Lang::get('global.search_for')}}',
                    minimumInputLength: 2,
                }
            );
        });
    </script>
@endsection


