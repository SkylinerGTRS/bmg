@extends('admin_layouts.dashboard')
@section('header')
    <link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ Lang::get('global.districts')}}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">{{ Lang::get('global.districts')}}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div style="padding: 20px">
                            <a href="{{route('admin.locations.districts.add')}}"><button type="button" class="btn btn-info btn-rounded">{{ Lang::get('global.add')}}</button></a>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <button class="btn btn-warning waves-effect waves-light collapseble" style="float: right;margin-top: -40px"><span>{{ Lang::get('global.filter')}}</span> <i class="fa fa-filter m-l-5"></i></button>
                                <div class="m-t-15 collapseblebox dn">
                                    <div class="well">
                                        <div class="col-12">
                                            <div class="form-group col-sm-6">
                                                <label for="RegionID" class="control-label">{{ Lang::get('global.regions')}}</label>
                                                <select id="RegionID" name="DistrictID" class="form-control select2" style="width:100%;">
                                                    <option value="">{{Lang::get('global.choose')}}</option>
                                                    @foreach($Regions as $item)
                                                        <option value="{{$item->id}}">{{$item->title_ka}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="search-title" class="control-label">{{ Lang::get('global.search_by_title')}}</label>
                                                <input class="form-control" id="search-title" type="text">
                                            </div>
                                        </div>
                                        <div class="row mt-8">
                                            <div class="col-lg-12">
                                                <button class="btn btn-primary btn-primary--icon" id="dt_search" style="float: right">
													<span>
														<i class="fa fa-search"></i>
														<span>Search</span>
													</span>
                                                </button>&nbsp;&nbsp;
                                                <button class="btn btn-secondary btn-secondary--icon" id="dt_reset" style="float: right; margin-right: 20px">
													<span>
														<i class="fa fa-close"></i>
														<span>Reset</span>
													</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title - Ka</th>
                                    <th>Title - Ka</th>
                                    <th>Title - ru</th>
                                    <th style="width: 250px !important;">Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer text-center"> 2020 &copy; All rights reserved <a href="https://cgroup.ge">Cgroup.ge</a> </footer>
    </div>
@endsection
@section('footer')
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            var table = $('#myTable').DataTable({
                processing : true,
                serverSide: true,
                searching: false,
                ordering: false,
                ajax: {
                    url: '{{route('admin.locations.districts')}}',
                    type: 'POST',
                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr("content")
                    },
                    'data': function(data){
                        // Append to data
                        data.type_id = $('#RegionID').val();
                        data.title =  $('#search-title').val();
                        data.PostTable = 'districts';
                    },

                },
                columns: [
                    { data: 'id' },
                    { data: 'title_ka' },
                    { data: 'title_en' },
                    { data: 'title_ru' },
                    { data: 'actions' },
                ]
            });
            $('#dt_search').click( function(e) {
                e.preventDefault();
                table.draw();
            });

            $('#dt_reset').click( function(e) {
                //e.preventDefault();
                location.reload();
            });
        });
    </script>
@endsection


