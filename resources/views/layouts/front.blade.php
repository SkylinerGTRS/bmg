<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('messages.js') }}"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Cgroup.ge">
    <meta name="robots" content="all" />
    <meta name="copyright" content="&copy;2020 mbg.ge" />
    <link rel="icon" href="/front_assets/img/logo.png" type="image/png" sizes="16x16">
    <meta property="og:url" content="@yield('ogurl')" />
    <meta property="og:title" content="@yield('ogtitle')" />
    <meta property="og:image" content="@yield('ogimage')"/>
    <meta property="og:image:width" content="500"/>
    <meta property="og:image:height" content="500"/>
    <meta property="og:site_name" content="mbg.ge"/>
    <meta property="og:description" content="@yield('ogdescription')"/>
    <meta property="og:type" content="website"/>
    @yield('header')
    <link rel="stylesheet" href="/front_assets/css/bootstrap.css">
    <link rel="stylesheet" href="/front_assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/front_assets/css/aos.css">
    <link rel="stylesheet" href="/front_assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/front_assets/css/style.css">
    <script type="text/javascript">
        var LANG = '{{ localization()->getCurrentLocale() }}';
        var URL = '{{ env('APP_URL') }}';
    </script>
</head>
<body>
    <header>
    <div class="container">
        <div class="wrapper">
            <div class="d-flex">
                <a href="/">
                    <img src="/front_assets/img/Logo.svg" alt="">
                    <h1 class="d-none">{{Lang::get('global.mbg_intro')}}</h1>
                </a>
                <ul class="menu">
                    <li><a href="{{route('front.home')}}">{{Lang::get('global.home')}}</a></li>
                    <li><a href="{{route('front.statements')}}">{{Lang::get('global.statements')}}</a></li>
                    <li><a href="{{route('front.services')}}">{{Lang::get('global.services')}}</a></li>
                    <li><a href="{{route('front.about')}}">{{Lang::get('global.about_us')}}</a></li>
                    <li><a href="{{route('front.blog')}}">{{Lang::get('global.blog')}}</a></li>
                    <li><a href="{{route('front.advertisement')}}">{{Lang::get('global.advertisement')}}</a></li>
                    <li><a href="{{route('front.contact')}}">{{Lang::get('global.contact')}}</a></li>
                </ul>
            </div>
            <div class="rightside">
                <div class="dropdown">
                    <ul>
                        @foreach($supportedLocales as $key => $locale)
                            <li class="{{ localization()->getCurrentLocale() == $key ? 'active' : '' }}">
                                <a href="{{ localization()->getLocalizedURL($key) }}" rel="alternate" hreflang="{{ $key }}">
                                    {{ $locale->native() }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                @if (\Request::route()->getName() != 'front.statements.add')
                    <a href="{{route('front.statements.add')}}" class="danger-btn">{{Lang::get('global.add_statement')}}<img src="/front_assets/img/plus.svg" alt=""></a>
                @endif
                <div class="hidden-1340">
                    <div class="menu-bar">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="mob-menu hidden-1340">
                <button class="cancel">
                    <img src="/front_assets/img/cancel.svg" alt="">
                </button>
                <ul class="menu">
                    <li><a href="{{route('front.home')}}">{{Lang::get('global.home')}}</a></li>
                    <li><a href="{{route('front.statements')}}">{{Lang::get('global.statements')}}</a></li>
                    <li><a href="{{route('front.services')}}">{{Lang::get('global.services')}}</a></li>
                    <li><a href="{{route('front.about')}}">{{Lang::get('global.about_us')}}</a></li>
                    <li><a href="{{route('front.blog')}}">{{Lang::get('global.blog')}}</a></li>
                    <li><a href="{{route('front.advertisement')}}">{{Lang::get('global.advertisement')}}</a></li>
                    <li><a href="{{route('front.contact')}}">{{Lang::get('global.contact')}}</a></li>
                </ul>
                <div class="socials">
                    <ul>
                        <li><a href="#"><img src="/front_assets/img/f.svg" alt=""></a></li>
                        <li><a href="#"><img src="/front_assets/img/i.svg" alt=""></a></li>
                        <li><a href="#"><img src="/front_assets/img/l.svg" alt=""></a></li>
                    </ul>
                </div>
                <div class="mob-contact">
                    <a href="mailto:{{$Contact->email}}">
                        <img src="/front_assets/img/mail.svg" alt="">
                        {{$Contact->email}}
                    </a>
                    <a href="tel:{{$Contact->phone}}">
                        <img src="/front_assets/img/headphones.svg" alt="">
                        {{$Contact->phone}}; {{$Contact->phone1}}
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
    @yield('content')
    <footer>
        <div class="container footer-container d-flex justify-between align-end relative">
            <img src="/front_assets/img/footerback.svg" alt="" class="back">
            @if (\Request::route()->getName() != 'front.statements.add')
            <a href="{{route('front.statements.add')}}" class="danger-btn w-fit">{{Lang::get('global.add_statement')}}<img src="/front_assets/img/plus.svg" alt=""></a>
            @endif
            <div class="footer-contact">
                <a href="mailto:{{$Contact->email}}"><img src="/front_assets/img/mail.svg" alt="">{{$Contact->email}}</a>
                <a href="tel:{{$Contact->phone}}"><img src="/front_assets/img/headphones.svg" alt="">{{$Contact->phone}}; {{$Contact->phone1}}</a>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="wrapper">
                    <p>{{Lang::get('global.mbg_intro')}} © {{Lang::get('global.all_rights')}}</p>
                    <ul>
                        <li><a href="{{$Contact->fb_link}}"><img src="/front_assets/img/f.svg" alt=""></a></li>
                        <li><a href="{{$Contact->ins_link}}"><img src="/front_assets/img/i.svg" alt=""></a></li>
                        <li><a href="{{$Contact->lin_link}}"><img src="/front_assets/img/l.svg" alt=""></a></li>
                    </ul>
                    <span>{{Lang::get('global.made_by')}} <a href="https://www.cgroup.ge/"><img src="/front_assets/img/cgroup.svg" alt=""></a> {{Lang::get('global.by')}}</span>
                </div>
            </div>
        </div>
    </footer>
    <script async src="/front_assets/js/share.js"></script>
@yield('footer')
    <script src="/front_assets/js/jquery.min.js"></script>
    <script>
        $(function(){
            // initialize fb sdk
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '830561340319450',
                    xfbml      : true,
                    version    : 'v2.2'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

        });
    </script>

    <script src="/front_assets/js/owl.carousel.js"></script>
    <script src="/front_assets/js/aos.js"></script>
    <script src="/front_assets/js/jquery-ui.min.js"></script>
    <script src="/front_assets/js/script.js"></script>
</body>
</html>
