<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3>
            <span class="fa-fw open-close">
                <i class="ti-close ti-menu"></i>
            </span>
            <span class="hide-menu">Admin Panel</span>
            </h3>
        </div>
        <ul class="nav" id="side-menu" style="margin-top: 60px">
            <li>
                <a href="{{route('admin_home')}}" class="waves-effect">
                    <i class="mdi mdi-av-timer fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.dashboard')}}</span>
                </a>
            </li>
            @can('view blog')
            <li>
                <a href="{{route('admin.blog')}}" class="waves-effect">
                    <i class="mdi mdi-blogger fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.blog')}}</span>
                </a>
            </li>
            @endcan
            @can('view services')
            <li>
                <a href="{{route('admin.services')}}" class="waves-effect">
                    <i class="mdi mdi-access-point-network fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.services')}}</span>
                </a>
            </li>
            @endcan
            @can('view properties')
            <li>
                <a href="{{route('admin.property_types')}}" class="waves-effect">
                    <i class="mdi mdi-home-variant fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.property_types')}}</span>
                </a>
            </li>
            @endcan
            @can('view prattrs')
            <li> <a href="#" class="waves-effect"><i class="mdi mdi-clipboard-text fa-fw"></i> <span class="hide-menu">{{ Lang::get('global.property_attrs')}}<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    @foreach($MainCats as $item)
                        <li><a href="{{route('admin.property_attrs',$item->cat_id)}}"><i class="fa-fw"> >> </i><span class="hide-menu">{{ $item->title}}</span></a></li>
                    @endforeach
                </ul>
            </li>
            @endcan
            @can('view deals')
            <li>
                <a href="{{route('admin.deal_types')}}" class="waves-effect">
                    <i class="mdi mdi-blender fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.deal_types')}}</span>
                </a>
            </li>
            @endcan
            @can('view locations')
            <li> <a href="#" class="waves-effect"><i class="mdi mdi-crosshairs-gps fa-fw"></i> <span class="hide-menu">{{ Lang::get('global.locations')}}<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{route('admin.locations.regions')}}"><i class="fa-fw"> > </i><span class="hide-menu">{{ Lang::get('global.regions')}}</span></a></li>
                    <li><a href="{{route('admin.locations.districts')}}"><i class="fa-fw"> > </i><span class="hide-menu">{{ Lang::get('global.districts')}}</span></a></li>
                    <li><a href="{{route('admin.locations.urban')}}"><i class="fa-fw"> > </i><span class="hide-menu">{{ Lang::get('global.urban')}}</span></a></li>
                    <li><a href="{{route('admin.locations.streets')}}"><i class="fa-fw"> > </i><span class="hide-menu">{{ Lang::get('global.streets')}}</span></a></li>
                </ul>
            </li>
            @endcan
            @can('view orders')
                <li> <a href="#" class="waves-effect"><i class="mdi mdi-archive fa-fw"></i> <span class="hide-menu">{{Lang::get('global.orders')}}<span class="label label-rouded label-danger pull-right">{{$NotChecked}}</span><span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{route('admin.orders.notchecked')}}"><i class="mdi mdi-alert-octagon fa-fw"></i><span class="hide-menu">{{ Lang::get('global.notchecked')}}<span class="label label-rouded label-danger pull-right">{{$NotChecked}}</span></span></a></li>
                        <li><a href="{{route('admin.orders.checked')}}"><i class="mdi mdi-bookmark-check fa-fw"></i><span class="hide-menu">{{ Lang::get('global.checked')}}<span class="label label-rouded label-success pull-right">{{$Checked}}</span></span></a></li>
                    </ul>
                </li>
            @endcan
            @can('view products')
            <li>
                <a href="{{route('admin.products')}}" class="waves-effect">
                    <i class="mdi mdi-blur-linear fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.statements')}}</span>
                </a>
            </li>
            @endcan
            @can('view about')
            <li>
                <a href="{{route('admin.about')}}" class="waves-effect">
                    <i class="mdi mdi-alert-circle-outline fa-fw"></i>
                    <span class="hide-menu">{{ Lang::get('global.about_us')}}</span>
                </a>
            </li>
            @endcan
            @can('view contact')
            <li>
                <a href="{{route('admin.contact.edit',1)}}" class="waves-effect">
                    <i class="mdi mdi-contact-mail fa-fw"></i>
                    <span class="hide-menu"> {{ Lang::get('global.contact')}} </span>
                </a>
            </li>
            @endcan
            @can('view users')
                <li>
                    <a href="{{route('admin.users')}}" class="waves-effect">
                        <i class="mdi mdi-account-multiple-plus fa-fw"></i>
                        <span class="hide-menu"> {{ Lang::get('global.users')}} </span>
                    </a>
                </li>
            @endcan
            @can('view translations')
            <li>
                <a href="{{route('admin.translations')}}" class="waves-effect">
                    <i class="mdi mdi-language-python fa-fw"></i>
                    <span class="hide-menu"> {{ Lang::get('global.translations')}} </span>
                </a>
            </li>
            @endcan
            @can('view meta')
            <li>
                <a href="{{route('admin.meta')}}" class="waves-effect">
                    <i class="mdi mdi-code-tags-check fa-fw"></i>
                    <span class="hide-menu"> {{ Lang::get('global.meta')}} </span>
                </a>
            </li>
            @endcan
        </ul>
    </div>
</div>
