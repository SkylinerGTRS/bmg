@extends('layouts.front')
@section('title'){{$item->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($item->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$item->title ?? 'MBG'}}@endsection
@if (isset($item->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($item->descr) ? Str::limit(strip_tags($item->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="services-detail services-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-9">
                    <div class="big img-frame">
                        <img src="/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}" alt="" class="img">
                    </div>
                    <span class="date">{{ Date::parse($item->created_at)->format('j M')}}</span>
                    <div class="title">
                        <h2>{{$item->title}}</h2>
                        <div class="share">
                            <span>{{Lang::get('global.share')}}</span>
                            <img src="/front_assets/img/long-arr.svg" alt="" class="arr">
                            <ul>
                                <li><img src="/front_assets/img/fgrey.svg" alt=""></li>
                                <li><img src="/front_assets/img/linkgrey.svg" alt=""></li>
                                <li><img src="/front_assets/img/twgrey.svg" alt=""></li>
                                <li><img src="/front_assets/img/vwgrey.svg" alt=""></li>
                                <li><img src="/front_assets/img/pingrey.svg" alt=""></li>
                                <li><img src="/front_assets/img/mailgrey.svg" alt=""></li>
                            </ul>
                        </div>
                    </div>
                    <div class="text">
                        <p>
                            {!! $item->descr !!}
                        </p>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="services">
                        @foreach ($Services as $item)
                            <a href="{{route('front.services.detail',[$item->id, $item->title])}}" class="box">
                                <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">
                                <span class="title">{{$item->title}}</span>
                                <div class="text">
                                    <p>{{Str::limit(strip_tags($item->descr), 100, '...')}}
                                    </p>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
