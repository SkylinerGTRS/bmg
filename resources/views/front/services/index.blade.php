@extends('layouts.front')
@section('title'){{$Meta->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($Meta->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$Meta->title ?? 'MBG'}}@endsection
@if (isset($Meta->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$Meta->MainImage['route_name']}}/large/{{$Meta->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($Meta->descr) ? Str::limit(strip_tags($Meta->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="services-page">
        <div class="container">
            <div class="main-title">{{Lang::get('global.services')}}<span>{{Lang::get('global.services')}}</span></div>
            <div class="row">
                @foreach ($Services as $item)
                    <div class="col-xl-6 col-lg-6">
                        <a href="{{route('front.services.detail',[$item->id,App\Helpers\Helpers::FUrl($item->title)])}}" class="block">
                            <div class="left">
                            <span>
                                @if (isset($item->MainImage))
                                    <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">
                                @endif
                            </span>
                            </div>
                            <div class="right">
                                <h5>{{$item->title}}</h5>
                                <div class="text">
                                    <p>{{Str::limit(strip_tags($item->descr), 400, '...')}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
