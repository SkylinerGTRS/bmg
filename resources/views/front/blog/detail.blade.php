@extends('layouts.front')
@section('title'){{$item->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($item->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$item->title ?? 'MBG'}}@endsection
@if (isset($item->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($item->descr) ? Str::limit(strip_tags($item->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="services-detail services-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-9">
                    <div class="big img-frame">
                        @if ($item->MainImage)
                            <img src="/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}" alt="" class="img">
                        @endif
                    </div>
                    <span class="date">{{ Date::parse($item->created_at)->format('j M')}}</span>
                    <div class="title">
                        <h2>{{$item->title}}</h2>
                        <div class="share">
                            <span>{{Lang::get('global.share')}}</span>
                            <img src="/front_assets/img/long-arr.svg" alt="" class="arr">
                            @include('front.includes.share')
                        </div>
                    </div>
                    <div class="text">
                        <p>{!! $item->descr !!}
                        </p>
                    </div>
                    <div class="blog">
                        <div class="blog-title title-wrapper">
                            <span>{{Lang::get('global.similar_posts')}}</span>
                            <a href="{{route('front.blog')}}" class="full">{{Lang::get('global.see_all')}}
                                <img src="/front_assets/img/next.svg" alt="">
                            </a>
                        </div>
                        <div class="row">
                            @foreach($Blogs as $item)
                                <div class="col-xl-4 col-lg-6 col-md-6">
                                    <a href="{{route('front.blog.detail',[$item->id, $item->title])}}" class="box">
                                        <div class="img-frame">
                                            @if ($item->MainImage)
                                                <img src="/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}" alt="" class="img">
                                            @endif
                                        </div>
                                        <div class="desc">
                                            <span class="date">{{ Date::parse($item->created_at)->format('j M')}}</span>
                                            <h2>{{$item->title}}</h2>
                                            <div class="text">
                                                <p>{{Str::limit(strip_tags($item->descr), 200, '...')}}
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="services">
                        <a href="#" class="box">
                            <img src="/front_assets/img/exclusive.svg" alt="">
                            <span class="title">ექსკლუზიური  შეთავაზება</span>
                            <div class="text">
                                <p>შემთხვევითად გენერირებული ტექსტი ეხმარებ დიზაინერებს და
                                    ტიპოგრაფიული ნაწარმის დაა შემქმნელებს, რეალურთან
                                    მაქსიმალურად მიახლოებული შაბლონი წარუდგინონ შემფასებელს.
                                    ხშირადაა შემთხვევა, როდესაც დიზაინის შესრულებისას
                                    საჩვენებელია, თუ როგორი იქნება ტექსტის ბლოკი. სწორედ ასეთ
                                    დროს არის მოსახერხებელი
                                </p>
                            </div>
                        </a>
                        <a href="#" class="box">
                            <img src="/front_assets/img/rent.svg" alt="">
                            <span class="title">ქონების შეფასება</span>
                            <div class="text">
                                <p>შემთხვევითად გენერირებული ტექსტი ეხმარებ დიზაინერებს და
                                    ტიპოგრაფიული ნაწარმის დაა შემქმნელებს, რეალურთან
                                    მაქსიმალურად მიახლოებული შაბლონი წარუდგინონ შემფასებელს.
                                    ხშირადაა შემთხვევა, როდესაც დიზაინის შესრულებისას
                                    საჩვენებელია, თუ როგორი იქნება ტექსტის ბლოკი. სწორედ ასეთ
                                    დროს არის მოსახერხებელი
                                </p>
                            </div>
                        </a>
                        <a href="#" class="box">
                            <img src="/front_assets/img/real.svg" alt="">
                            <span class="title">რეალიზაცია</span>
                            <div class="text">
                                <p>შემთხვევითად გენერირებული ტექსტი ეხმარებ დიზაინერებს და
                                    ტიპოგრაფიული ნაწარმის დაა შემქმნელებს, რეალურთან
                                    მაქსიმალურად მიახლოებული შაბლონი წარუდგინონ შემფასებელს.
                                    ხშირადაა შემთხვევა, როდესაც დიზაინის შესრულებისას
                                    საჩვენებელია, თუ როგორი იქნება ტექსტის ბლოკი. სწორედ ასეთ
                                    დროს არის მოსახერხებელი
                                </p>
                            </div>
                        </a>
                        <a href="#" class="box">
                            <img src="/front_assets/img/legal.svg" alt="">
                            <span class="title">იურიდიული, საბანკო მომსახურება</span>
                            <div class="text">
                                <p>შემთხვევითად გენერირებული ტექსტი ეხმარებ დიზაინერებს და
                                    ტიპოგრაფიული ნაწარმის დაა შემქმნელებს, რეალურთან
                                    მაქსიმალურად მიახლოებული შაბლონი წარუდგინონ შემფასებელს.
                                    ხშირადაა შემთხვევა, როდესაც დიზაინის შესრულებისას
                                    საჩვენებელია, თუ როგორი იქნება ტექსტის ბლოკი. სწორედ ასეთ
                                    დროს არის მოსახერხებელი
                                </p>
                            </div>
                        </a>
                        <a href="#" class="box">
                            <img src="/front_assets/img/photo.svg" alt="">
                            <span class="title">ფოტო და ვიდეო გადაღება</span>
                            <div class="text">
                                <p>შემთხვევითად გენერირებული ტექსტი ეხმარებ დიზაინერებს და
                                    ტიპოგრაფიული ნაწარმის დაა შემქმნელებს, რეალურთან
                                    მაქსიმალურად მიახლოებული შაბლონი წარუდგინონ შემფასებელს.
                                    ხშირადაა შემთხვევა, როდესაც დიზაინის შესრულებისას
                                    საჩვენებელია, თუ როგორი იქნება ტექსტის ბლოკი. სწორედ ასეთ
                                    დროს არის მოსახერხებელი
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
