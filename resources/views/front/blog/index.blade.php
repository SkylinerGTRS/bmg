@extends('layouts.front')
@section('title'){{$Meta->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($Meta->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$Meta->title ?? 'MBG'}}@endsection
@if (isset($Meta->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$Meta->MainImage['route_name']}}/large/{{$Meta->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($Meta->descr) ? Str::limit(strip_tags($Meta->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="blog-page">
        <div class="container">
            <div class="main-title">{{Lang::get('global.blog')}}<span>{{Lang::get('global.blog')}}</span></div>
            <div class="content-cont">
                <div class="leftside">
                    <img src="/uploads/{{$MainBlog->MainImage['route_name']}}/large/{{$MainBlog->MainImage['name']}}" alt="">
                </div>
                <div class="rightside">
                    <span class="date">{{ Date::parse($MainBlog->created_at)->format('j M')}}</span>
                    <h2>{{$MainBlog->title}}</h2>
                    <div class="text">
                        <p>{{Str::limit(strip_tags($MainBlog->descr), 500, '...')}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="blog">
                <div class="row">
                    @foreach ($Blog as $item)
                        <div class="col-xl-3">
                            <a href="{{route('front.blog.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="box">
                                <div class="img-frame">
                                    @if (isset($item->MainImage))
                                    <img src="/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}" alt="" class="img">
                                    @endif
                                </div>
                                <div class="desc">
                                    <span class="date">{{ Date::parse($item->created_at)->format('j M')}}</span>
                                    <h2>{{$item->title}}</h2>
                                    <div class="text">
                                        <p>{{Str::limit(strip_tags($MainBlog->descr), 200, '...')}}
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>
                {{ $Blog->links('vendor.pagination.default') }}
            </div>
        </div>
    </section>
@endsection
