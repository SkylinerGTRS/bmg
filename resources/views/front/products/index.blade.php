@extends('layouts.front')
@section('title'){{$Meta->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($Meta->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$Meta->title ?? 'MBG'}}@endsection
@if (isset($Meta->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$Meta->MainImage['route_name']}}/large/{{$Meta->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($Meta->descr) ? Str::limit(strip_tags($Meta->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="search-page">
        <div class="intro">
            <img src="/front_assets/img/day.svg " alt="">
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-4">
                        <div class="filter">
                            <form action="{{route('front.statements.search')}}" method="GET">
                                <div class="input">
                                    <img src="/front_assets/img/grey-loup.svg" alt="">
                                    <input type="text" placeholder="ID, {{Lang::get('global.word')}}" name="Word" value="{{$Word ?? ''}}" class="not">
                                </div>
                                <span class="filt-title">{{Lang::get('global.price')}}</span>
                                <div class="rangeslider">
                                    <div class="value-container">
                                        <input type="hidden" name="SelectedFromVal" class="fromVal" data-value="{{$SelectedFromVal ?? ''}}">
                                        <input type="hidden" name="SelectedToVal" class="toVal" data-value="{{$SelectedToVal ?? ''}}">
                                        <span name="MinPrice" id="minPrice" class="value left rangeVal" data-value="{{$MinPrice}}">₾ {{$MinPrice}}</span>
                                        <span name="MaxPrice" id="maxPrice" class="value right rangeVal" data-value="{{$MaxPrice}}">₾ {{$MaxPrice}}</span>
                                        <!-- <button type="button"><img src="/front_assets/img/small-arr.svg" alt=""></button> -->
                                    </div>
                                    <div id="slider"></div>
                                </div>
                                <div class="checkbox-list">
                                    <span class="filt-title">{{Lang::get('global.deal_type')}}</span>
                                    <ul>
                                        @foreach($DealTypes as $deal)
                                        <li>
                                            <button type="button">
                                                <label>
                                                    {{$deal->title}}
                                                    <input type="checkbox" name="Deal[]" value="{{$deal->cat_id}}"
                                                    @if (isset($SelectedDeal) && in_array($deal->cat_id, $SelectedDeal))
                                                        checked
                                                    @endif
                                                    >
                                                </label>
                                            </button>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="checkbox-list">
                                    <span class="filt-title">{{Lang::get('global.property_type')}}</span>
                                    <ul>
                                        @foreach($PropertyTypes as $pr)
                                            <li>
                                                <button type="button">
                                                    <label>
                                                        {{$pr->title}}
                                                        <input type="checkbox" name="Property[]" value="{{$pr->cat_id}}"
                                                            @if (isset($SelectedProperty) && in_array($pr->cat_id, $SelectedProperty))
                                                               checked
                                                            @endif
                                                        >
                                                    </label>
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="checkbox-list">
                                    <span class="filt-title">{{Lang::get('global.location')}}</span>
                                    <ul>
                                        @foreach($Location as $loc)
                                        <li>
                                            <button type="button">
                                                <label>
                                                    {{ $loc->{'title_'. localization()->getCurrentLocale()} }}
                                                    <input type="checkbox" name="Location[]" value="{{$loc->id}}"
                                                        @if (isset($SelectedLocation) && in_array($loc->id, $SelectedLocation))
                                                           checked
                                                        @endif
                                                    >
                                                </label>
                                            </button>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <span class="filt-title">{{Lang::get('global.space')}}</span>
                                <div class="wrapper">
                                    <div class="input-form">
                                        <label for="">
                                            <span>{{Lang::get('global.from')}}</span>
                                            <input type="number" name="SpaceFrom" min="1" max="10000" value="{{$SelectedSpaceFrom ?? ''}}">
                                        </label>
                                    </div>
                                    <div class="input-form">
                                        <label for="">
                                            <span>{{Lang::get('global.to')}}</span>
                                            <input type="number" name="SpaceTo" min="1" max="10000" value="{{$SelectedSpaceTo ?? ''}}">
                                        </label>
                                    </div>
                                </div>
                                <div class="wrapper btn-wrap">
                                    <button type="button" onclick="location.href = '{{route('front.statements')}}';" class="submit-btn clear">{{Lang::get('global.clear')}}</button>
                                    <button type="submit" class="submit-btn">{{Lang::get('global.search')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-8">
                        <div class="row">
                            <!-- banner  -->
                            <div class="col-xl-12">
                                <div class="banner mb-20" style="background-image: url(/front_assets/img/back.png);">
                                    <div>
                                        {!! Lang::get('global.add_st_with_no_rg')!!}
                                    </div>
                                    <a href="{{route('front.statements.add')}}" class="danger-btn z-1">{{Lang::get('global.add_statement')}}<img src="/front_assets/img/plus.svg" alt=""></a>
                                </div>
                            </div>
                            @if(count($Products) > 0)
                            @foreach($Products as $item)
                                <div class="col-xl-4 col-lg-6 col-md-6">
                                    <div class="main-box mb-20">
                                        <a href="{{route('front.statements.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="img-frame">
                                            @if (isset($item->MainImage))
                                                <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">
                                            @endif
                                        </a>
                                        <div class="desc">
                                            <div class="top">
                                                <h6 class="grey-title">{{$item->Property->title}}</h6>
                                                <div class="right">
                                                    <span class="grey-title">{{$item->space}} კვ.</span>
                                                </div>
                                            </div>
                                            <a href="#">
                                                <h2>{{$item->title}}</h2>
                                            </a>
                                            <a href="{{route('front.statements.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="location">
                                                <img src="/front_assets/img/location.svg" alt="">
                                                {{ $item->Urban->{'title_'.localization()->getCurrentLocale()} }}
                                            </a>
                                            <div class="d-flex">
                                                <span class="price">{{$item->price_gel}}</span>
                                                <div class="switch">
                                                    <input type="checkbox" checked="true">
                                                    <span class="slider round"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                                @else
                                <div class="col-xl-4 col-lg-6 col-md-6">
                            {{Lang::get('global.no_items_found')}}
                                </div>
                                @endif
                        </div>
                        {{ $Products->links('vendor.pagination.default') }}
                        <a href="{{route('front.blog')}}" class="blog-banner">
                            <img src="/front_assets/img/blog.svg" alt="">
                            <div class="absolute">
                                <span>{{Lang::get('global.blog')}}</span>
                                <p>{{Lang::get('global.inner_blog_intro')}}</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
