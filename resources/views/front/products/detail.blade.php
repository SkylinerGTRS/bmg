@extends('layouts.front')
@section('title'){{$item->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($item->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$item->title ?? 'MBG'}}@endsection
@if (isset($item->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$item->MainImage['route_name']}}/large/{{$item->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($item->descr) ? Str::limit(strip_tags($item->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="detail-page">
        <div class="gallery">
            <div class="owl-container">
                <div class="owl-carousel owl-theme first">
                    @foreach($item->Images as $image)
                    <div class="item" data-id="{{$image->id}}">
                        <img src="/uploads/{{$image['route_name']}}/large/{{$image['name']}}" alt="">
                    </div>
                    @endforeach
                </div>
                <div class="synced-owl find-owl">
                    <div class="owl-carousel owl-theme sec">
                        @foreach($item->Images as $image)
                            <div class="item" data-id="{{$image->id}}">
                                <img src="/uploads/{{$image['route_name']}}/thumbs/{{$image['name']}}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <button class="slide-btn slide-prev"><img src="/front_assets/img/det-left.svg" alt=""></button>
                    <button class="slide-btn slide-next"><img src="/front_assets/img/det-right.svg" alt=""></button>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="return-page">
                <img src="/front_assets/img/link.svg" alt="" class="mr-5">
                <ul class="d-flex">
                    <li><a href="#">{{$item->title}} /</a></li>
                    <li><a href="#">{{ $item->urban->{'title_'.localization()->getCurrentLocale()} }}</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-xl-9">
                    <div class="content">
                        <div class="wrapper">
                            <div>
                                <h2 class="title">{{$item->title}}</h2>
                                <span class="location">
                                    <img src="/front_assets/img/red-pin.svg" alt="" class="mr-5">{{ $item->urban->{'title_'.localization()->getCurrentLocale()} }}</span>
                            </div>
                            <div class="views">
                                <span><img src="/front_assets/img/eye.svg" alt="" class="mr-10">{{$item->views}}</span>
                                <span><b class="mr-5">ID:</b>{{$item->id}}</span>
                            </div>
                        </div>
                        <div class="det-info">
                            <ul>
                                <li>
                                    <img src="/front_assets/img/square.svg" alt="">
                                    {{$item->space}}<sup>მ2</sup>
                                </li>
                                @foreach ($MainCats as $cat)
                                    <li>
                                        <img src="/images/{{$cat->MainCat->image}}" alt="" style="height: 20px">
                                        {{$cat->MainCat->title}} - {{$cat->value}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="desc">
                            <span class="title">{{Lang::get('global.aditional_info')}}</span>
                            <div class="text">
                                <p>
                                   {!! $item->descr !!}
                                </p>
                            </div>
                        </div>
                        <div class="grid">
                            <ul>
                                @foreach ($ProductCats as $attr)
                                    <li>
                                        <img src="/front_assets/img/confirmed.svg" alt="" class="mr-10">{{$attr->Category->title}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="title-wrapper mb-20">
                        <div class="wrapper">
                            <div class="left">
                                <span class="title">{{Lang::get('global.same_offers')}}</span>
                                <a href="{{route('front.statements')}}" class="full">{{Lang::get('global.see_all')}}
                                    <img src="/front_assets/img/next.svg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($SameProducts as $samePr)
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="main-box">
                                <a href="{{route('front.statements.detail',[$samePr->id,App\Helpers\Helpers::FUrl($samePr->title)])}}" class="img-frame">
                                    @if ($samePr->MainImage)
                                        <img src="/uploads/{{$samePr->MainImage['route_name']}}/thumbs/{{$samePr->MainImage['name']}}" alt="">
                                    @endif
                                </a>
                                <div class="desc">
                                    <div class="top">
                                        <h6 class="grey-title">{{$samePr->Property->title}}</h6>
                                        <div class="right">
                                            <span class="grey-title">{{$samePr->space}} m2</span>
                                        </div>
                                    </div>
                                    <a href="{{route('front.statements.detail',[$samePr->id,App\Helpers\Helpers::FUrl($samePr->title)])}}">
                                        <h2>{{$samePr->title}}</h2>
                                    </a>
                                    <a href="#" class="location">
                                        <img src="/front_assets/img/location.svg" alt="">
                                        {{ $samePr->Urban->{'title_'.localization()->getCurrentLocale()} }}
                                    </a>
                                    <div class="d-flex">
                                        <span class="price">{{$samePr->price_gel}}</span>
                                        <div class="switch">
                                            <input type="checkbox" checked="true">
                                            <span class="slider round"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="blog">
                        <div class="blog-title title-wrapper">
                            <span>{{Lang::get('global.blog')}}</span>
                            <a href="{{route('front.blog')}}" class="full">{{Lang::get('global.see_all')}}
                                <img src="/front_assets/img/next.svg" alt="">
                            </a>
                        </div>
                        <div class="row">
                            @foreach ($Blog as $sB)
                                <div class="col-xl-4 col-lg-4 col-md-6">
                                    <a href="{{route('front.blog.detail',[$sB->id, App\Helpers\Helpers::FUrl($sB->title)])}}" class="box">
                                        <div class="img-frame">
                                            @if ($sB->MainImage)
                                                <img src="/uploads/{{$sB->MainImage['route_name']}}/thumbs/{{$sB->MainImage['name']}}" alt="">
                                            @endif
                                        </div>
                                        <div class="desc">
                                            <span class="date">{{ Date::parse($sB->created_at)->format('j M')}}</span>
                                            <h2>{{$sB->title}}</h2>
                                            <div class="text">
                                                <p>{{Str::limit(strip_tags($sB->descr), 150, '...') }}
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="price-wrap">
                        <div class="price">
                            {{$item->price_gel}}
                        </div>
                        <div class="switch">
                            <input type="checkbox" checked="true">
                            <span class="slider round"></span>
                        </div>
                    </div>
                    <div class="share">
                        <span>{{Lang::get('global.share')}}</span>
                        <img src="/front_assets/img/long-arr.svg" alt="" class="arr">
                        @include('front.includes.share')
                    </div>
                    <a href="tel:{{$Contact->phone}}" class="call-us">
                        <img src="/front_assets/img/white-logo.svg" alt="">
                        <div>
                            <span>{{Lang::get('global.mbg_group')}}</span>
                            <span><img src="/front_assets/img/white-phone.svg" alt="">{{$Contact->phone}}</span>
                        </div>
                    </a>
                    <div class="services d-block">
                        @foreach ($Services as $item)
                        <a href="{{route('front.services.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="box d-block mb-20">
                            <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">
                            <span class="title">{{$item->title}}</span>
                            <div class="text">
                                <p>{{Str::limit(strip_tags($item->descr), 150, '...') }}
                                </p>
                            </div>
                        </a>
                        @endforeach
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
