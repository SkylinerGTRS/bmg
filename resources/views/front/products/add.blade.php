@extends('layouts.front')
@section('title'){{$Meta->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($Meta->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$Meta->title ?? 'MBG'}}@endsection
@if (isset($Meta->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$Meta->MainImage['route_name']}}/large/{{$Meta->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($Meta->descr) ? Str::limit(strip_tags($Meta->descr), 100, '...') : 'MBG'}}@endsection
@section('header')
    <link href="/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <script>
        let Data = '';
    </script>
@endsection
@section('content')
    <section class="add-page">
        <div class="container">
            <div class="main-title">{{Lang::get('global.add_your_statement')}}<span>{{Lang::get('global.add_your_statement')}}</span></div>
            <div class="warning-box">
                <p>
                    {{Lang::get('global.statement_add_warning')}}
                </p>
            </div>
            <form action="{{route('front.statements.add')}}" method="POST" onsubmit="return front.savePost(this)">
                @CSRF
                <div class="row">
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="text" placeholder="{{Lang::get('global.name')}}" name="Name" class="input" required>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="text" placeholder="{{Lang::get('global.lastname')}}" name="Lastname" class="input" required>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="number" placeholder="{{Lang::get('global.phone')}}" name="Phone" class="input" required>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="email" placeholder="{{Lang::get('global.email')}}" name="Email" class="input" required>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="text" placeholder="{{Lang::get('global.code')}}" name="Code" class="input" required>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 d-flex">
                                <input type="text" placeholder="{{Lang::get('global.price')}}"  name="Price" class="input" required>
                                <div class="dropdown">
                                    <button type="button" class="selected">
                                        <span class="text">₾</span>
                                        <img src="/front_assets/img/arr.svg" alt="" class="ml-5">
                                        <input type="hidden" name="currency" value="gel">
                                    </button>
                                    <div class="result">
                                        <ul>
                                            <li data-value="gel">₾</li>
                                            <li data-value="usd">$</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <textarea placeholder="{{Lang::get('global.full_descr')}}" name="Message"></textarea>
                    </div>
                </div>
                <span class="up-title">{{Lang::get('global.maximum_photos')}}</span>
                <div class="upload">
                    <div class="dropzone" id="my-awesome-dropzone">
                        <button type="button" class="select">
                            <label>
                                {{Lang::get('global.chose_or_drop_files')}}
                                <img src="/front_assets/img/plus.svg" alt="">
                            </label>
                        </button>
                    </div>
                </div>
                <label class="accept-agreement">{{Lang::get('global.agree_with_terms')}} <input type="checkbox" name="Terms" required value="1"></label>
                <button class="success-btn">{{Lang::get('global.add')}}</button>
            </form>
        </div>
    </section>

@endsection
@section('footer')
    <script src="/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
    <script type="text/javascript" data-accepted_files=".jpeg,.jpg,.png" data-PostTable="orders" data-max_files="15" src="/admin_assets/js/file.js"></script>
    <script>
        $(document).ready(function(){
            $('.upload .select').click(function () {
                $(this).parents('.dropzone').trigger('click');
            })
        });
    </script>
@endsection
