@extends('layouts.front')
@section('title'){{$Meta->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($Meta->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$Meta->title ?? 'MBG'}}@endsection
@if (isset($Meta->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$Meta->MainImage['route_name']}}/large/{{$Meta->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($Meta->descr) ? Str::limit(strip_tags($Meta->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
    <section class="about-us">
        <div class="container">
            <div class="main-title">{{Lang::get('global.about_us')}}<span>{{Lang::get('global.about_us')}}</span></div>
            <div class="text">
                <p>{!! $MainAbout->descr!!}
                </p>
            </div>
            <div class="row">
                @foreach ($About as $item)
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="box">
                            <a href="#" class="img-frame">
                                @if (isset($item->MainImage))
                                    <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="" class="img">
                                @endif
                                <span>{{Lang::get('global.see_more')}}</span>
                            </a>
                            <a href="#" class="name">{{$item->title}}</a>
                            <span class="sub">{{$item->position}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
