@extends('layouts.front')
@section('title'){{$Meta->title ?? 'MBG'}}@endsection
@section('description'){{ Str::limit(strip_tags($Meta->descr), 100, '...') }}@endsection
@section('ogurl'){{URL::current()}}@endsection
@section('ogtitle'){{$Meta->title ?? 'MBG'}}@endsection
@if (isset($Meta->MainImage))
@section('ogimage'){{ env('APP_URL') }}/uploads/{{$Meta->MainImage['route_name']}}/large/{{$Meta->MainImage['name']}}@endsection
@endif
@section('ogdescription'){{ isset($Meta->descr) ? Str::limit(strip_tags($Meta->descr), 100, '...') : 'MBG'}}@endsection
@section('content')
<section>
    <div class="intro">
        <form action="">
            <div class="search">
                <div class="dropdown">
                    <button class="selected" type="button">
                        <span class="title">გარიგების ტიპი<img src="/front_assets/img/downarrow.svg" alt=""></span>
                        <span class="text">აირჩიე</span>
                    </button>
                    <div class="result">
                        <ul>
                            <li>იყიდება</li>
                            <li>ქირავდება</li>
                            <li>გირავდება</li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown">
                    <button class="selected" type="button">
                        <span class="title">გარიგების ტიპი<img src="/front_assets/img/downarrow.svg" alt=""></span>
                        <span class="text">აირჩიე</span>
                    </button>
                    <div class="result">
                        <ul>
                            <li>იყიდება</li>
                            <li>ქირავდება</li>
                            <li>გირავდება</li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown">
                    <button class="selected" type="button">
                        <span class="title">გარიგების ტიპი<img src="/front_assets/img/downarrow.svg" alt=""></span>
                        <span class="text">აირჩიე</span>
                    </button>
                    <div class="result">
                        <ul>
                            <li>იყიდება</li>
                            <li>ქირავდება</li>
                            <li>გირავდება</li>
                        </ul>
                    </div>
                </div>
                <input type="text" class="not" placeholder="ID, სიტყვა">
                <button class="search-btn">მოძებნე<img src="/front_assets/img/loupe.svg" alt=""></button>
            </div>
        </form>
        <div class="art">
            <img src="/front_assets/img/art.svg" alt="">
        </div>
    </div>
    <div class="container pt-50">
        <div class="services">
            @foreach ($Services as $item)
                <a href="{{route('front.services.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="box">
                    @if ($item->MainImage)
                        <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">
                    @endif

                    <span class="title">{{$item->title}}</span>
                    <div class="text">
                        <p>
                            {{Str::limit(strip_tags($item->descr), 90, '...') }}
                        </p>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
    <div class="mb-50 float-slider find-owl">
        <div class="title-wrapper">
            <div class="container float-cust">
                <div class="wrapper">
                    <div class="left">
                        <span class="title">{!! Lang::get('global.exc_offers')!!}</span>
                        <a href="#" class="full">{!! Lang::get('global.see_all')!!}
                            <img src="/front_assets/img/next.svg" alt="">
                        </a>
                    </div>
                    <div class="slider-arrows">
                        <button class="slide-prev">
                            <img src="/front_assets/img/pre.svg" alt="">
                        </button>
                        <button class="slide-next">
                            <img src="/front_assets/img/nex.svg" alt="">
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="float-container">
            <div class="owl-carousel owl-theme">
                @foreach($ExcStatements as $item)
                <div class="item">
                    <div class="main-box">
                        <a href="{{route('front.statements.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="img-frame">
                            @if (isset($item->MainImage))
                            <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">
                                @endif
                        </a>
                        <div class="desc">
                            <div class="top">
                                <h6 class="grey-title">{{$item->Property->title}}</h6>
                                <div class="right">
{{--                                    <span class="grey-title">3 ოთახი</span>--}}
                                    <span class="grey-title">{{$item->space}} კვ.</span>
                                </div>
                            </div>
                            <a href="{{route('front.statements.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}">
                                <h2>{{$item->title}}</h2>
                            </a>
                            <a href="#" class="location">
                                <img src="/front_assets/img/location.svg" alt="">
                                {{ $item->Urban->{'title_'.localization()->getCurrentLocale()} }}
                            </a>
                            <div class="d-flex">
                                <span class="price">{{$item->price_gel}}</span>
                                <div class="switch">
                                    <input type="checkbox" checked="true">
                                    <span class="slider round"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @foreach($Properties as $property)
        <div class="mb-50 float-slider find-owl">
            <div class="title-wrapper">
                <div class="container float-cust">
                    <div class="wrapper">
                        <div class="left">
                            <span class="title">{{$property->title}}</span>
                            <a href="#" class="full">{!! Lang::get('global.see_all')!!}
                                <img src="/front_assets/img/next.svg" alt="">
                            </a>
                        </div>
                        <div class="slider-arrows">
                            <button class="slide-prev">
                                <img src="/front_assets/img/pre.svg" alt="">
                            </button>
                            <button class="slide-next">
                                <img src="/front_assets/img/nex.svg" alt="">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="float-container">
                <div class="owl-carousel owl-theme">
                    @foreach($property->Statements as $statement)
                        <div class="item">
                            <div class="main-box">
                                <a href="{{route('front.statements.detail',[$statement->id, App\Helpers\Helpers::FUrl($statement->title)])}}" class="img-frame">
                                    @if (isset($statement->MainImage))
                                        <img src="/uploads/{{$statement->MainImage['route_name']}}/thumbs/{{$statement->MainImage['name']}}" alt="">
                                    @endif
                                </a>
                                <div class="desc">
                                    <div class="top">
                                        <h6 class="grey-title">{{$statement->Property->title}}</h6>
                                        <div class="right">
                                            {{--                                    <span class="grey-title">3 ოთახი</span>--}}
                                            <span class="grey-title">{{$statement->space}} კვ.</span>
                                        </div>
                                    </div>
                                    <a href="{{route('front.statements.detail',[$statement->id, App\Helpers\Helpers::FUrl($statement->title)])}}">
                                        <h2>{{$statement->title}}</h2>
                                    </a>
                                    <a href="#" class="location">
                                        <img src="/front_assets/img/location.svg" alt="">
                                        {{ $statement->Urban->{'title_'.localization()->getCurrentLocale()} }}
                                    </a>
                                    <div class="d-flex">
                                        <span class="price">{{$statement->price_gel}}</span>
                                        <div class="switch">
                                            <input type="checkbox" checked="true">
                                            <span class="slider round"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
{{--    <div class="mb-50 float-slider find-owl">--}}
{{--        <div class="title-wrapper">--}}
{{--            <div class="container float-cust">--}}
{{--                <div class="wrapper">--}}
{{--                    <div class="left">--}}
{{--                        <span class="title"><b>Vip</b> შეთავაზებები</span>--}}
{{--                        <a href="#" class="full">{{Lang::get('global.see_all')}}--}}
{{--                            <img src="/front_assets/img/next.svg" alt="">--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="slider-arrows">--}}
{{--                        <button class="slide-prev">--}}
{{--                            <img src="/front_assets/img/pre.svg" alt="">--}}
{{--                        </button>--}}
{{--                        <button class="slide-next">--}}
{{--                            <img src="/front_assets/img/nex.svg" alt="">--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="float-container">--}}
{{--            <div class="owl-carousel owl-theme">--}}
{{--                @foreach($VipStatements as $item)--}}
{{--                    <div class="item">--}}
{{--                        <div class="main-box">--}}
{{--                            <a href="#" class="img-frame">--}}
{{--                                @if (isset($item->MainImage))--}}
{{--                                    <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="">--}}
{{--                                @endif--}}
{{--                            </a>--}}
{{--                            <div class="desc">--}}
{{--                                <div class="top">--}}
{{--                                    <h6 class="grey-title">{{$item->Property->title}}</h6>--}}
{{--                                    <div class="right">--}}
{{--                                        --}}{{--                                    <span class="grey-title">3 ოთახი</span>--}}
{{--                                        <span class="grey-title">{{$item->space}} კვ.</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <a href="#">--}}
{{--                                    <h2>{{$item->title}}</h2>--}}
{{--                                </a>--}}
{{--                                <a href="#" class="location">--}}
{{--                                    <img src="/front_assets/img/location.svg" alt="">--}}
{{--                                    {{ $item->urban->{'title_'.localization()->getCurrentLocale()} }}--}}
{{--                                </a>--}}
{{--                                <div class="d-flex">--}}
{{--                                    <span class="price">{{$item->price_gel}}</span>--}}
{{--                                    <div class="switch">--}}
{{--                                        <input type="checkbox" checked="true">--}}
{{--                                        <span class="slider round"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="add-app">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 left-wrap">
                    <div class="title-wrap">
                        {!! Lang::get('global.add_st_intro')!!}
                    </div>
                    <div class="text">
                        <p>
                            {!! Lang::get('global.add_st_intro_descr')!!}
                        </p>
                    </div>
                </div>
                <div class="col-xl-7 offset-xl-1">
                    <div class="frame-wrap">
                        <div class="col-xl-7 relative">
                            <div class="img-frame big">
                                <img src="/front_assets/img/phot1.png" alt="" class="img">
                            </div>
                            <a href="{{route('front.statements.add')}}"><button class="add-btn">{!! Lang::get('global.add_statement')!!}<img src="/front_assets/img/plus.svg" alt="">
                                </button></a>
                            <div class="img-frame sm">
                                <img src="/front_assets/img/phot3.png" alt="" class="img">
                            </div>
                        </div>
                        <div class="col-xl-5">
                            <div class="img-frame right">
                                <img src="/front_assets/img/phot2.png" alt="" class="img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="new-apps">
        <div class="title-wrapper mb-20">
            <div class="container">
                <div class="wrapper">
                    <div class="left">
                        <span class="title">{!! Lang::get('global.newly_added')!!}</span>
                        <a href="#" class="full">{!! Lang::get('global.see_all')!!}
                            <img src="/front_assets/img/next.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="grid">
                @foreach ($NewStatements as $item)
                    <div class="main-box">
                        <a href="{{route('front.statements.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="img-frame">
                            @if ($item->MainImage)
                            <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="" class="img">
                            @endif
                        </a>
                        <div class="desc">
                            <a href="{{route('front.statements.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}">
                                <h2>{{$item->title}}</h2>
                            </a>
                            <div class="d-flex">
                                <span class="price" style="display: none">{{$item->price_gel}}</span>
                                <span class="price">{{$item->price_usd}}</span>
                                <div class="switch">
                                    <input type="checkbox" checked="true">
                                    <span class="slider round"></span>
                                </div>
                            </div>
                            <a href="#" class="location">
                                <img src="/front_assets/img/location.svg" alt="">
                                {{ $item->Urban->{'title_'.localization()->getCurrentLocale()} }}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="blog">
        <div class="blog-title">
            <span>{!! Lang::get('global.blog')!!}</span>
            <div class="text">
                <p>{!! Lang::get('global.blog_intro')!!}</p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach ($Blog as $item)
                    <div class="col-xl-4 col-md-6">
                        <a href="{{route('front.blog.detail',[$item->id, App\Helpers\Helpers::FUrl($item->title)])}}" class="box">
                            <div class="img-frame">
                                @if ($item->MainImage)
                                    <img src="/uploads/{{$item->MainImage['route_name']}}/thumbs/{{$item->MainImage['name']}}" alt="" class="img">
                                @endif

                            </div>
                            <div class="desc">
                                <span class="date">{{ Date::parse($item->created_at)->format('j M')}}</span>
                                <h2>{{$item->title}}</h2>
                                <div class="text">
                                    <p>
                                        {{Str::limit(strip_tags($item->descr), 150, '...') }}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="center">
            <a href="{{route('front.blog')}}">{!! Lang::get('global.see_more')!!}<img src="/front_assets/img/right-arrow.svg" alt=""></a>
        </div>
    </div>
    <div class="location-bl">
        <div class="title-wrapper">
            <div class="container">
                <div class="wrapper">
                    <div class="left">
                        <span class="title">{!! Lang::get('global.search_in_loc')!!}</span>
                        <a href="#" class="full">{!! Lang::get('global.see_all')!!}
                            <img src="/front_assets/img/next.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>თბილისი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">1516</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>ბათუმი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">1248</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>ახალციხე</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">978</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>რუსთავი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">245</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>ქუთაისი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">198</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>ქობულეთი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">145</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>ზუგდიდი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">98</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <a href="#" class="box">
                        <h4>გორი</h4>
                        <img src="/front_assets/img/batumi.png" alt="">
                        <div class="inner">
                            <span class="danger">26</span>
                            <span class="sm-title">განცხადება</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
