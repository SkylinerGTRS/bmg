<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <ul>
        <li><a class="a2a_button_facebook" data-url="{{Request::Url()}}"><img src="/front_assets/img/fgrey.svg"/></a></li>
        <li><a class="a2a_button_linkedin" data-url="{{Request::Url()}}"><img src="/front_assets/img/linkgrey.svg" alt=""></a></li>
        <li><a class="a2a_button_twitter" data-url="{{Request::Url()}}"><img src="/front_assets/img/twgrey.svg" alt=""></a></li>
        <li><a class="a2a_button_vk" data-url="{{Request::Url()}}"><img src="/front_assets/img/vwgrey.svg" alt=""></a></li>
        <li><a class="a2a_button_pinterest" data-url="{{Request::Url()}}"><img src="/front_assets/img/pingrey.svg" alt=""></a></li>
        <li><a class="a2a_button_email" data-url="{{Request::Url()}}"><img src="/front_assets/img/mailgrey.svg" alt=""></a></li>
    </ul>
</div>
