<?php
return [
    'deleted_status_id' => 2,
    'active_status_id' => 1,
    'inactive_status_id' => 0,
    'LANGS' => serialize(array('en' => 1, 'ka' => 2, 'ru' => 3)),
    'IMG_TYPES' => serialize(array('image/png', 'image/jpg', 'image/jpeg', 'image/gif', 'image/svg+xml')),
    'IMG' => serialize(array('png', 'jpg', 'jpeg')),
    'DOC' => serialize(array('doc', 'docx', 'pdf','xls','xlsx')),
    //ROOMS
    'UPLOADS' =>[
        'category' => [
            'LARGE_DIR' => '/uploads/category/large/',
            'THUMBS_DIR' => '/uploads/category/thumbs/',
            'SVG_DIR' => '/uploads/category/svg/',
            'LARGE_WIDTH' => '1200',
            'LARGE_HEIGHT' => '800',
            'THUMBS_WIDTH' => '600',
            'THUMBS_HEIGHT' => '350',
        ],
        'contact' => [
            'LARGE_DIR' => '/uploads/contact/large/',
            'THUMBS_DIR' => '/uploads/contact/thumbs/',
            'LARGE_WIDTH' => '1200',
            'LARGE_HEIGHT' => '800',
            'THUMBS_WIDTH' => '600',
            'THUMBS_HEIGHT' => '350',
        ],
        //products
        'products' => [
            'LARGE_DIR' => '/uploads/products/large/',
            'THUMBS_DIR' => '/uploads/products/thumbs/',
            'LARGE_WIDTH' => '1200',
            'LARGE_HEIGHT' => '800',
            'THUMBS_WIDTH' => '600',
            'THUMBS_HEIGHT' => '350',
        ],
        //service
        'services' => [
            'LARGE_DIR' => '/uploads/services/large/',
            'THUMBS_DIR' => '/uploads/services/thumbs/',
            'LARGE_WIDTH' => '1200',
            'LARGE_HEIGHT' => '800',
            'THUMBS_WIDTH' => '600',
            'THUMBS_HEIGHT' => '600',
        ],
        //blog
        'blog' => [
            'LARGE_DIR' => '/uploads/blog/large/',
            'THUMBS_DIR' => '/uploads/blog/thumbs/',
            'LARGE_WIDTH' => '865',
            'LARGE_HEIGHT' => '550',
            'THUMBS_WIDTH' => '273',
            'THUMBS_HEIGHT' => '180',
        ],

        //ABOUT
        'about' => [
            'LARGE_DIR' => '/uploads/about/large/',
            'THUMBS_DIR' => '/uploads/about/thumbs/',
            'LARGE_WIDTH' => '1200',
            'LARGE_HEIGHT' => '800',
            'THUMBS_WIDTH' => '400',
            'THUMBS_HEIGHT' => '500',
            ],

        //ORDERS
        'orders' => [
            'LARGE_DIR' => '/uploads/orders/large/',
            'THUMBS_DIR' => '/uploads/orders/thumbs/',
            'LARGE_WIDTH' => '1200',
            'LARGE_HEIGHT' => '800',
            'THUMBS_WIDTH' => '120',
            'THUMBS_HEIGHT' => '120',
        ],

        ]
];
